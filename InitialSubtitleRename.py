import os
import shutil
import re
import sys

if(len(sys.argv) != 2):
    print("Usage: python3 InitialSubtitleRename.py <path_to_subtitles_to_match>")
    exit()

# Define paths for files to compare
path1 = sys.argv[1]
path2 = './course/static'
srt_path = './course/static'
video_path = './course/video'

# Function to read first 3 lines of a file
def read_first_3_lines(file_path):
    with open(file_path, 'r') as f:
        lines = [next(f).rstrip() for x in range(3)]
    return lines

found = {}

# Loop through each file in the first path, including subdirectories
for root, dirs, files in os.walk(path1):
    for filename1 in files:
        # Only match files that end in .srt
        if not filename1.endswith('.srt'):
            continue

#        print(filename1)
        file1_path = os.path.join(root, filename1)
        file1_lines = read_first_3_lines(file1_path)
        match_found = False

        # Loop through each file in the second path
        for filename2 in os.listdir(path2):
            file2_path = os.path.join(path2, filename2)

            # Skip files that don't end in .srt
            if filename2.endswith('.srt'):
                file2_lines = read_first_3_lines(file2_path)
                # Compare first 3 lines of both files
                # We can apparently have false positives if we only use the timestamp from [1], but I suspect we'll have false negatives from using [2]
                if file1_lines[1] == file2_lines[1] and file1_lines[2] == file2_lines[2]:
#                    print(f"{filename1} matches {filename2}")
#                    print(f"('{filename2[:-7]}'), ('{filename1[:-4]}')")
                    sanitized_name = filename1[:-4].encode('ascii', 'ignore').decode() + "__v1"

                    # Store the result for printing out in sorted form after the fact
                    found[filename2[:-7]] = sanitized_name

                    # Delete the randomized-name file, and copy the matching human-readable-named file there instead
#                    print(f"Removing {file2_path}")
                    os.remove(file2_path)
                    file_out = file2_path.replace(filename2[:-7], sanitized_name)
                    shutil.copy(file1_path, file_out)
                    print(f"{file2_path} renamed to {file_out}");

                    match_found = True
                    break

        if not match_found:
            print(f"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            print(f"No match found for {filename1}, {file1_lines[1]}")
            print(f"You probably forgot to upload that video to your class! (Or the first line of text changed.) Re-upload it to the class, re-export the class, and try again.")
            print(f"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

# Sort the dictionary by the value (the human-readable name) incase we want to print it
sorted_found = sorted(found.items(), key=lambda x: x[1])

#for old_name, original_name in sorted_found:
#    print(f"('{old_name}', '{original_name}'),")

def replace_strings_in_XML(root_dir):
    for subdir, dirs, files in os.walk(root_dir):
        for filename in files:
            if filename.endswith('.xml'):
                filepath = os.path.join(subdir, filename)
                with open(filepath, 'r+') as f:
                    lines = f.readlines()
                    f.seek(0)
                    f.truncate()
                    for line in lines:
                        for old_name, new_name in sorted_found:
                            pattern = re.compile(re.escape(old_name))
                            line = re.sub(pattern, new_name, line)
                        f.write(line)

# Call the function to increment the numbers in a directory
replace_strings_in_XML(video_path)
