# Beta tester guidance

Thanks for helping beta test this class! Below are some of the things we're looking for help with:

 * How long does the course take? **We need you to track how much time you spend on the class, in a text file on your computer.** But there's different guidance for how many examples to do, depending on what kind of student you are:
    * **If you are a developer**, you're recommended to watch **at least 3** examples, where one should be marked as "vulnerability + exploitation" (🥷). You get to pick which examples you want to watch. And then you can watch more than 3 after you've finished that minimum requirement, if you feel like seeing more examples would help your understanding of the problem.
    * **If you are an aspiring vulnerability-hunter/exploiter**, you need to watch **all** examples in order to build up your pattern-recognition skills, and your times should reflect that (don't skip any.)
    * Note: The reason we ask that you keep track of your time in a file on your computer is because we will probably add a few more examples after the class starts. And we'll need you to then update your time in the timing questions within the class. And this will be easiest if you know how much time you previously spent on any sections you need to go back to and do more examples within.


 * You are ***allowed and encouraged*** to try and use ChatGPT to see if it can find bugs. We're curious. If you have success, let us know in the Discussion section below the relevant vulnerability.

 * Are there any broken links? Typos? Copy-paste errors? - If so, let us know in the discussions section at the bottom of the relevant page.

 * Are any citations from the [slides](https://gitlab.com/opensecuritytraining/vulns1002_c_implementation_vulns_slides_and_subtitles) missing in the website? (It's fine for the website to have more than the slides, just not vice-versa.)

 * Does the page say there are no proof of concepts (PoCs) available, but there actually are? - If so, submit a link in the discussions section at the bottom of the relevant page.

 * Are there any mistakes in the video transcripts? (Hint: There **will** be initially! ;) We depend on students crowdsourcing the fixes, so we can instead spend our time making new class content.) You can find how to submit fixes in the next unit.

 * Also, you need to click the "mark as complete" buttons throughout the class as you go. While Open edX has an auto-completion-tracking mechanism, it's unreliable, and so this will be the best way for you to keep track of your progress in the class (which you can review in the "Progress" faux-tab at the top of the page.)

 * Please provide accurate feedback about whether you found the flaw in exercises, and how difficult you found it. This will allow me to fine-tune difficulty within the class.

 * Finally, this class involves more complicated vulnerabilities, which cannot easily fit into the summary text box on the website anymore. Therefore you'll need to look around in checked out copies of the real code. You can use whatever code viewing mechanism you're most comfortable with, but if you don't have a strong preference, an optional guide to using Eclipse is provided in an upcoming section within this class.