## Read The Fun Code!

An easy thing to spot in code, is whether the developer knows to program paranoid with respect to dangling pointers. This is based on whether you see the free-and-NULL⚔️ style used.

If you do:  

 * Did they miss any locations \[1]?
 
If you don't:  

 * Do you see any Words of Power like "retain" and "release", "garbage collection", and "mutual exclusion" that indicate that you are looking at dangerous code from a race-based-UAF perspective?
 * If not, then you'll just need to gain a deep understand of the code, and look for erroneous control flow paths that can cause a premature free() through other means.


\[1] While **I** don't currently know how to do this, I suspect finding this sort of thing is a task best fit for using a code query language, such as [CodeQL](https://codeql.github.com/) or [SemGrep](https://github.com/0xdea/semgrep-rules). I believe it should be possible to define a query that's roughly like "find any location where free() is called, without an assignment to NULL of the parameter that was passed into free()".

(If any **beta testers** want to write a blog post discussing the efficacy of such things, for me to link to, LMK in the discussion once it's created. If you do that, LMK how [this rule](https://github.com/0xdea/semgrep-rules/blob/main/c/use-after-free.yaml) works too.)