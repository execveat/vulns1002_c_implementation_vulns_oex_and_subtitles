# Fix

When you have found the vuln, you can diff the code to find the fix by downloading version 18.05 here:

[https://sourceforge.net/projects/sevenzip/files/7-Zip/18.05/7z1805-src.7z/download](https://sourceforge.net/projects/sevenzip/files/7-Zip/18.05/7z1805-src.7z/download)

There is also a description of the diffs that relate to the fix [here](https://landave.io/files/patch_7zip_CVE-2018-10115.txt), by the original discoverer.

As always, you should ask yourself: is it sufficient? Did they miss a corner case? Is there some other code path that can yield the same core bug?

# Exploitation

If you will be learning exploitation from OST2 or elsewhere in the future, it is recommended to not read too much from the original advisory about how this can be exploited. Rather, hold it in reserve, and later on you can try to exploit it yourself. Then, if you can't, you can treat the details in the advisory as a hint for how to go about doing the exploitation, before writing the exploit yourself.