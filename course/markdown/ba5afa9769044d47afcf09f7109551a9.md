## Links from video

[https://docs.microsoft.com/en-us/windows/win32/gdi/windows-gdi](https://docs.microsoft.com/en-us/windows/win32/gdi/windows-gdi)

[IDA Pro](https://hex-rays.com/ida-pro/) software reverse engineering (SRE) tool. [Hex Rays Decompiler](https://hex-rays.com/decompiler/) plugin for IDA Pro.

[BinDiff](https://www.zynamics.com/bindiff.html) binary difference analysis tool.

[gpsi type citation](https://cbwang505.gitee.io/win32k/a00020.html).

[(tag)WNDCLASSEXA](https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-wndclassexa) structure definition.

[SetWindowLongPtrA()](https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-setwindowlongptra) function definition.

[Known GDI trick](https://www.coresecurity.com/sites/default/files/private-files/
publications/2016/10/Abusing-GDI-Reloaded-ekoparty-2016_0.pdf) for causing info leak.

Subsequent discussion of [mitigation](https://labs.f-secure.com/archive/a-tale-of-bitmaps/) for GDI info leak in newering Windows OSes.

Known "Windows kernel token stealing payload" exploit privilege escalation technique discussions: [example 1](https://connormcgarr.github.io/x64-Kernel-Shellcode-Revisited-and-SMEP-Bypass/), [example 2](https://hshrzd.wordpress.com/2017/06/22/starting-with-windows-kernel-exploitation-part-3-stealing-the-access-token/).