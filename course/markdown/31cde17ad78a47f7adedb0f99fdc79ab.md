## Flaw

This has been [CVE-2020-14364](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-14364), brought to you by Yanyu Zhang [@f1yYY__](https://twitter.com/f1yYY__), Lingni Kong, and Haipeng Qu.

You can read the original advisory describing the problem [here](https://conference.hitb.org/hitbsecconf2021ams/materials/D2T2%20-%20A%20Black%20Box%20Escape%20Of%20Qemu%20Based%20On%20The%20USB%20Device%20-%20L.%20Kong,%20Y.%20Zhang%20&%20H.%20Qu.pdf), and the video for that presentation is [here](https://www.youtube.com/watch?v=dHZSAiLKvSY).

# Exploitation

If you will be learning exploitation from OST2 or elsewhere in the future, it is recommended to not read too much from the original advisory about how this can be exploited. Rather, hold it in reserve, and later on you can try to exploit it yourself. Then, if you can't, you can treat the details in the advisory as a hint for how to go about doing the exploitation, before writing the exploit yourself.