# Code

Download the source for 7-zip 18.03 from here:

[https://sourceforge.net/projects/sevenzip/files/7-Zip/18.03/7z1803-src.7z/download](https://sourceforge.net/projects/sevenzip/files/7-Zip/18.03/7z1803-src.7z/download)

The irony being that you will need something that supports 7-zip decompression in order to decompress the source. 

E.g. Ubuntu you can use:  

 * ```sudo apt-get install p7zip-full```
 * ```mkdir foo```
 * ```mv 7z1803-src.7z foo```
 * ```cd foo```
 * ```p7zip -d 7z1803-src.7z -k```
