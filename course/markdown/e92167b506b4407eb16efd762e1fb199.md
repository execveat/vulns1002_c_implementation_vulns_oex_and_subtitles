## Most important keyboard shortcuts

The below are the default command shortcuts. They can be remapped in Preferences -> General -> Keys if desired.  
⭐️ = the most important commands that you need to memorize.

| Essential Commands					| Windows 				| Linux 				| macOS 				| 
| -----------      						| -----------			| ----------- 			| ----------- 			|
| ⭐️Search				 				| control-h				| control-h				| control-h				|
| ⭐️Go to definition 					| F3 or control-click	| F3 or control-click	| F3 or command-click	|
| ⭐️Find all references 				| control-shift-g		| control-shift-g		| command-shift-g		|
| ⭐️grep for *selected* text				| control-alt-g			| control-alt-g			| command-option-g		|
| ⭐️Open call hierarchy 				| control-alt-h			| control-alt-h 		| control-option-h		|
| ⭐️Navigate backwards					| alt-left-arrow		| alt-left-arrow		| command-\[			|
| ⭐️Navigate forwards					| alt-right-arrow		| alt-right-arrow		| command-\]			|

| Nice-To-Have Commands					| Windows 				| Linux 				| macOS 				| 
| -----------      						| -----------			| ----------- 			| ----------- 			|
| Go to matching brace/bracket			| control-shift-p		| control-shift-p 		| command-shift-p		|
| Next occurrence of *selected* text	| control-k				| control-k				| command-k				|
| Previous occurrence of *selected* text| control-shift-k		| control-shift-k 		| command-shift-k		|
| Toggle folding enablement				| control-numpad-divide	| control-numpad-divide	| command-numpad-divide	|
| Fold single entry						| control-numpad-minus	| control-numpad-minus 	| command-numpad-minus	|
| Unfold single entry					| control-numpad-plus	| control-numpad-plus 	| command-numpad-plus	|
| Refactor (change definition everywhere)| shift-alt-r			| shift-alt-r 			| command-option-r		|

---
## Creating a new workspace

When you're ready to create new workspaces later on in the class, you can do this through File -> Switch Workspace -> Other, and then just change then path from "qemu_CVE-2021-3608" (or whatever else is already there), and put the new name for the new workspace.

---

