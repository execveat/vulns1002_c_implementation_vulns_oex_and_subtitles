## Fuzzing

Fuzzing *can* be a somewhat effective way to detect race conditions. However, it should not be assumed that just throwing a stock fuzzer against something will necessarily yield race conditions. In general many race conditions which are found through fuzzing are the result of a researcher tailoring a fuzzing system to find this type of flaw. (E.g. \[1]\[2]\[3], just randomly picked as top Google results, not necessarily top results for relevance.)

**Developers**: your best bet is to ask any security engineers that you have that support you, to see if they can tailor your fuzzing environment for increased race condition applicability. If you don't have security engineers that can provide this support, you should still be running a fuzzer, because of all the past vulnerability applicability that you've already seen in Vulns1001. But here you'd just be looking to "get lucky" and catch a race in action. (Note however that "lucky" races found through fuzzing can be even harder than normal results to reproduce.)

**Vuln hunters**: your best bet is to explore the related research, and try to adapt an existing race-capable fuzzing system to a new target that it's never been applied to before. (Because of course if you just use the fuzzer against the same target you *may* get lucky and find that the target code has been updated, and no one bothered to run the fuzzer again. But you're just as likely to find that all the bugs that the system is capable of finding, have already be found.) Alternatively you could apply new techniques to the existing system and then run it again. Either way, the key point is that you likely need to introduce some additional novelty to find additional bugs.

\[1] [Catch Me If You Can: Deterministic Discovery of Race Conditions with Fuzzing](https://i.blackhat.com/USA-22/Thursday/US-22-williamson-Catch-Me-If-You-Can.pdf)  
\[2] [Context-Sensitive and Directional Concurrency Fuzzing for Data-Race Detection](https://www.ndss-symposium.org/wp-content/uploads/2022-296-paper.pdf)  
\[3] [Razzer: Finding Kernel Race Bugs through Fuzzing](https://lifeasageek.github.io/papers/jeong-razzer.pdf)  
