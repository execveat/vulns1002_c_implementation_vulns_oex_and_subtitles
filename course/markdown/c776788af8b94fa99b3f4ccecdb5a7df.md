# Code

Checkout the vulnerable code as follows:

```
git clone https://github.com/torvalds/linux.git linux_CVE-2019-2215

cd linux_CVE-2019-2215

git checkout 16ae30ea17cdd2b67f486c3518592067c8f9cc62
```

---

**Hint 0:** The vulnerability is in the Android-specific "Binder" mechanism.


Now go hunt in the code for a UAF vuln.