# Fix

When you have found the vuln, you can view the patch [here](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=9bf292bfca94694a721449e3fd752493856710f6).

As always, you should ask yourself: is it sufficient? Did they miss a corner case? Is there some other code path that can yield the same core bug?