# Fix

When you have found the vuln, you can view the patch [here](https://android.googlesource.com/kernel/msm/+/550c01d0e051461437d6e9d72f573759e7bc5047%5E%21/#F0).

As always, you should ask yourself: is it sufficient? Did they miss a corner case? Is there some other code path that can yield the same core bug?

Hint: We know from [here](https://googleprojectzero.github.io/0days-in-the-wild/0day-RCAs/2019/CVE-2019-2215.html), "The patch for the in-the-wild 0-day (CVE-2019-2215) actually introduced another use-after-free condition"...