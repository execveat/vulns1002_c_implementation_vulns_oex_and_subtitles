## Flaw

This has been [CVE-2016-6130](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-6130) as found by [Pengfei Wang](https://wpengfei.github.io/) (social media handle unknown), [et al.](https://www.usenix.org/conference/usenixsecurity17/technical-sessions/presentation/wang-pengfei)

You can read the original report describing the problem [here](https://bugzilla.kernel.org/show_bug.cgi?id=116741). This bug was found as part of [this research](https://www.usenix.org/conference/usenixsecurity17/technical-sessions/presentation/wang-pengfei) presented at USENIX 2017.