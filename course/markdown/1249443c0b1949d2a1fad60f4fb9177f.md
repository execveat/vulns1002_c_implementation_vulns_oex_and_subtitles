## Additional Reading

### Microsoft's addition of automatic-initialization options to its MSVC compiler

[Killing Uninitialized Memory: Protecting the OS Without Destroying Performance](https://github.com/microsoft/MSRC-Security-Research/blob/master/presentations/2019_09_CppCon/CppCon2019%20-%20Killing%20Uninitialized%20Memory.pdf), by Joe Bialek, Shayne Hiet-Block, Microsoft, Sept. 2019  
[Solving Uninitialized Stack Memory on Windows](https://msrc-blog.microsoft.com/2020/05/13/solving-uninitialized-stack-memory-on-windows/), by Joe Bialek, Microsoft, May 2020  
[Solving Uninitialized Kernel Pool Memory on Windows](https://msrc-blog.microsoft.com/2020/07/02/solving-uninitialized-kernel-pool-memory-on-windows/), by Joe Bialek, Microsoft, July 2020


### Using a static analysis tool for variant analysis

[MindShaRE: Analyzing BSD Kernels for Uninitialized Memory Disclosures Using Binary Ninja](https://www.zerodayinitiative.com/blog/2022/9/19/mindshare-analyzing-bsd-kernels-with-binary-ninja?rq=Uninitialized)

### Research-ware tools for detecting uninitialized data access

https://yajin.org/papers/ccs19_timeplayer.pdf