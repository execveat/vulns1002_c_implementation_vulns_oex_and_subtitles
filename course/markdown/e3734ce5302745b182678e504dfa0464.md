# Code

Checkout the vulnerable code from here:

[https://download.virtualbox.org/virtualbox/6.1.4/VirtualBox-6.1.4.tar.bz2](https://download.virtualbox.org/virtualbox/6.1.4/VirtualBox-6.1.4.tar.bz2)


---

**Hint 0:** The attack surface is SCSI command handling, as performed by the virtual SCSI controller hardware.


Now go hunt in the code for a UAF vuln.