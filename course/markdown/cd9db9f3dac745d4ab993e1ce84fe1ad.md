# Fix

When you have found the vuln, you can view the patch [here](https://gitlab.com/qemu-project/qemu/-/commit/813212288970c39b1800f63e83ac6e96588095c6).

As always, you should ask yourself: is it sufficient? Did they miss a corner case? Is there some other code path that can yield the same core bug?