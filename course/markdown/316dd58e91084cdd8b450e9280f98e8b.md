## Flaw

This has been [CVE-2021-3320](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-3320), brought to you (presumably) by the GitHub Security team (based on the fact that the advisory is labeled GHSA like other GitHub Security Advisories).

You can read the original advisory describing the problem [here](https://github.com/zephyrproject-rtos/zephyr/security/advisories/GHSA-27r3-rxch-2hm7).
