# Fix

When you have found the vuln, you can view the patch [here](https://github.com/qemu/qemu/commit/b946434f2659a182afc17e155be6791ebfb302eb).

As always, you should ask yourself: is it sufficient? Did they miss a corner case? Is there some other code path that can yield the same core bug?