1
00:00:00,240 --> 00:00:04,920
now I just want to point out one type of

2
00:00:02,760 --> 00:00:07,080
race condition the traditional file

3
00:00:04,920 --> 00:00:09,360
system based time of check time of use

4
00:00:07,080 --> 00:00:11,700
it's been discussed quite a bit since

5
00:00:09,360 --> 00:00:14,040
the early 2000s but it's specifically

6
00:00:11,700 --> 00:00:16,440
out of scope for this class not because

7
00:00:14,040 --> 00:00:18,900
it is in a race condition but because

8
00:00:16,440 --> 00:00:20,820
the mitigations and the behaviors and

9
00:00:18,900 --> 00:00:22,439
just the description of it doesn't sort

10
00:00:20,820 --> 00:00:24,359
of fit in with all the other types of

11
00:00:22,439 --> 00:00:26,279
bugs that we cover in this class in the

12
00:00:24,359 --> 00:00:28,560
previous class so we're mostly

13
00:00:26,279 --> 00:00:31,199
interested in race conditions that can

14
00:00:28,560 --> 00:00:33,000
cause memory corruptions reopen heap

15
00:00:31,199 --> 00:00:35,460
overflow stack overflows those kind of

16
00:00:33,000 --> 00:00:36,719
things and also cause the types of

17
00:00:35,460 --> 00:00:39,000
vulnerabilities you're about to learn

18
00:00:36,719 --> 00:00:41,879
about in the next sections such as use

19
00:00:39,000 --> 00:00:43,440
after free and type confusion so the

20
00:00:41,879 --> 00:00:45,719
type of race condition we cover in this

21
00:00:43,440 --> 00:00:47,579
class integrates well with all the other

22
00:00:45,719 --> 00:00:49,379
types of vulnerabilities whereas the

23
00:00:47,579 --> 00:00:52,320
type of race condition dealing with file

24
00:00:49,379 --> 00:00:54,780
systems is very operating system and

25
00:00:52,320 --> 00:00:56,579
file system dependent so the guidance

26
00:00:54,780 --> 00:00:57,960
that we would give you for that type of

27
00:00:56,579 --> 00:00:59,760
race condition wouldn't really have

28
00:00:57,960 --> 00:01:01,199
anything to do with the other types of

29
00:00:59,760 --> 00:01:04,140
race condition we're seeing in this

30
00:01:01,199 --> 00:01:05,880
class nevertheless just a quick show of

31
00:01:04,140 --> 00:01:07,080
what I'm talking about here if you

32
00:01:05,880 --> 00:01:09,180
weren't familiar with this kind of thing

33
00:01:07,080 --> 00:01:10,799
if you've never seen it before so

34
00:01:09,180 --> 00:01:13,439
traditionally for instance there's a

35
00:01:10,799 --> 00:01:15,060
system called access which is used to

36
00:01:13,439 --> 00:01:17,340
check whether you have access to a

37
00:01:15,060 --> 00:01:20,159
particular path a particular file at a

38
00:01:17,340 --> 00:01:22,680
particular path and if access is okay

39
00:01:20,159 --> 00:01:25,860
for read for instance then you go ahead

40
00:01:22,680 --> 00:01:28,380
and open that file with read permissions

41
00:01:25,860 --> 00:01:30,180
so the race condition here is that you

42
00:01:28,380 --> 00:01:34,020
could have userspace process one

43
00:01:30,180 --> 00:01:36,780
running it runs path name of blah and

44
00:01:34,020 --> 00:01:38,759
access check that calls up to the kernel

45
00:01:36,780 --> 00:01:41,640
it's a system call after all and the

46
00:01:38,759 --> 00:01:43,920
kernel says yes it's all good but then a

47
00:01:41,640 --> 00:01:46,439
context switch occurs and an attacker

48
00:01:43,920 --> 00:01:48,720
running elsewhere on the system might do

49
00:01:46,439 --> 00:01:52,020
something like remove slash temp slash

50
00:01:48,720 --> 00:01:54,960
blah and then link it with an ACID file

51
00:01:52,020 --> 00:01:57,540
and have that ACID link be named slash

52
00:01:54,960 --> 00:02:00,479
temp slash blah then when the context

53
00:01:57,540 --> 00:02:03,479
switches back to the first process it

54
00:02:00,479 --> 00:02:05,820
will open the path name but it turns out

55
00:02:03,479 --> 00:02:07,740
that now it is operating on ACID instead

56
00:02:05,820 --> 00:02:10,080
of the original temporary file that it

57
00:02:07,740 --> 00:02:13,440
expected so that's sort of a file system

58
00:02:10,080 --> 00:02:16,200
time of check at access time and time of

59
00:02:13,440 --> 00:02:18,360
use at open time in this particular case

60
00:02:16,200 --> 00:02:20,340
you can see this isn't exactly the sort

61
00:02:18,360 --> 00:02:22,560
of double fetch vulnerability like we

62
00:02:20,340 --> 00:02:24,300
were talking about before because in

63
00:02:22,560 --> 00:02:26,580
some sense the kernel you know just

64
00:02:24,300 --> 00:02:27,959
checks access permissions which is not

65
00:02:26,580 --> 00:02:30,360
the same thing as opening and

66
00:02:27,959 --> 00:02:32,280
potentially reading from the file so

67
00:02:30,360 --> 00:02:35,580
alternatively to that instead the

68
00:02:32,280 --> 00:02:38,220
attacker might take slash temp slash

69
00:02:35,580 --> 00:02:40,560
blah and link it to /etc/shadow

70
00:02:38,220 --> 00:02:42,840
which is a file they themselves may not

71
00:02:40,560 --> 00:02:44,879
have permission to open but then they've

72
00:02:42,840 --> 00:02:47,099
tricked this first process into opening

73
00:02:44,879 --> 00:02:49,920
it and operating on it and who knows

74
00:02:47,099 --> 00:02:51,420
maybe they somehow get the process to be

75
00:02:49,920 --> 00:02:53,819
tricked into changing a password or

76
00:02:51,420 --> 00:02:56,280
something like that so again this type

77
00:02:53,819 --> 00:02:58,019
of file system based race condition is

78
00:02:56,280 --> 00:03:00,300
not in scope for this class but it is

79
00:02:58,019 --> 00:03:03,379
potentially in scope for future OST2

80
00:03:00,300 --> 00:03:03,379
vulnerability classes

