1
00:00:00,000 --> 00:00:04,500
okay at this point you should have set

2
00:00:01,920 --> 00:00:05,759
up your first workspace and then now you

3
00:00:04,500 --> 00:00:07,020
want to figure out how to navigate

4
00:00:05,759 --> 00:00:09,540
around the code

5
00:00:07,020 --> 00:00:11,460
so where we would start here is that

6
00:00:09,540 --> 00:00:13,679
later on in the class when you're asked

7
00:00:11,460 --> 00:00:16,020
to check out some vulnerable code you

8
00:00:13,679 --> 00:00:18,359
will typically be also given some hint

9
00:00:16,020 --> 00:00:20,640
code to say here's the stuff you need to

10
00:00:18,359 --> 00:00:22,740
focus on assume that this is ACID assume

11
00:00:20,640 --> 00:00:25,140
that that is ACID so you're going to

12
00:00:22,740 --> 00:00:27,060
start by taking that hint code and

13
00:00:25,140 --> 00:00:28,920
you're going to go find that code in

14
00:00:27,060 --> 00:00:31,740
your workspace project

15
00:00:28,920 --> 00:00:34,020
so to do that we're going to run the

16
00:00:31,740 --> 00:00:36,960
first and most important command which

17
00:00:34,020 --> 00:00:39,960
is search so on Windows and Linux this

18
00:00:36,960 --> 00:00:41,460
is control h and on macOS this is also

19
00:00:39,960 --> 00:00:43,860
control h this is the only one that's

20
00:00:41,460 --> 00:00:45,660
the same between all of them well I

21
00:00:43,860 --> 00:00:46,620
suppose F3 is also the same but I never

22
00:00:45,660 --> 00:00:49,920
use that

23
00:00:46,620 --> 00:00:51,960
so we're going to hit Ctrl H

24
00:00:49,920 --> 00:00:53,760
and I've set this up so that this will

25
00:00:51,960 --> 00:00:56,039
show whenever I use commands so that

26
00:00:53,760 --> 00:00:57,899
it's obvious what exactly I just did and

27
00:00:56,039 --> 00:00:59,760
now the first thing I always do in my

28
00:00:57,899 --> 00:01:02,520
search window is that I hit this

29
00:00:59,760 --> 00:01:04,500
customize and I turn off git search

30
00:01:02,520 --> 00:01:06,420
because I only ever use file search

31
00:01:04,500 --> 00:01:08,580
which is effectively the same as just

32
00:01:06,420 --> 00:01:11,460
grupping through and doing a pure string

33
00:01:08,580 --> 00:01:16,200
search and C++ search

34
00:01:11,460 --> 00:01:19,080
so if you did a search for init Dev ring

35
00:01:16,200 --> 00:01:21,720
here this would just basically search

36
00:01:19,080 --> 00:01:24,240
everywhere that that particular pattern

37
00:01:21,720 --> 00:01:27,060
occurs in the system and so the search

38
00:01:24,240 --> 00:01:29,220
bar will appear down here in a tab

39
00:01:27,060 --> 00:01:31,380
there are options right here that are

40
00:01:29,220 --> 00:01:33,240
extremely useful for expand all and

41
00:01:31,380 --> 00:01:35,280
collapse all once we do things that have

42
00:01:33,240 --> 00:01:37,320
many more results those could be useful

43
00:01:35,280 --> 00:01:39,360
to just expand everything out here it's

44
00:01:37,320 --> 00:01:42,299
a nice small number of results so that

45
00:01:39,360 --> 00:01:44,759
was just a general search if we instead

46
00:01:42,299 --> 00:01:46,979
did the C++ search this would only

47
00:01:44,759 --> 00:01:49,860
show us things that we choose so we

48
00:01:46,979 --> 00:01:53,159
could say only show me variables or only

49
00:01:49,860 --> 00:01:55,860
show me declarations or definitions that

50
00:01:53,159 --> 00:01:57,540
kind of thing so this is useful to if

51
00:01:55,860 --> 00:01:59,340
you're trying to avoid perhaps some

52
00:01:57,540 --> 00:02:00,600
comments or something like that then

53
00:01:59,340 --> 00:02:03,659
that could be useful in this case

54
00:02:00,600 --> 00:02:05,460
there's a only a very infrequently found

55
00:02:03,659 --> 00:02:07,920
thing so it's actually three and three

56
00:02:05,460 --> 00:02:11,160
for both types of searches but so now

57
00:02:07,920 --> 00:02:13,260
we've gotten to the code here and we can

58
00:02:11,160 --> 00:02:15,540
for instance go to definitions of things

59
00:02:13,260 --> 00:02:18,540
we can hover over them and Eclipse will

60
00:02:15,540 --> 00:02:20,099
nicely give us a little pop-up to say

61
00:02:18,540 --> 00:02:21,840
like okay here's the actual definition

62
00:02:20,099 --> 00:02:23,879
of that struct and here's the definition

63
00:02:21,840 --> 00:02:25,739
of that struct and as you can see it's

64
00:02:23,879 --> 00:02:28,440
saying that if you hit F2 then you'll

65
00:02:25,739 --> 00:02:30,480
focus on it and it won't just disappear

66
00:02:28,440 --> 00:02:32,400
out from underneath you okay so

67
00:02:30,480 --> 00:02:34,800
searching is just the easy and obvious

68
00:02:32,400 --> 00:02:36,900
way to find your way around

69
00:02:34,800 --> 00:02:38,520
next we want to know how to go to

70
00:02:36,900 --> 00:02:40,140
definitions for things so we search our

71
00:02:38,520 --> 00:02:41,819
way into this and then we say okay well

72
00:02:40,140 --> 00:02:44,459
what is this function right here well I

73
00:02:41,819 --> 00:02:47,160
can hover over it and I'd like to see

74
00:02:44,459 --> 00:02:49,200
the actual full definition for it so

75
00:02:47,160 --> 00:02:52,739
there's a variety of ways we can do that

76
00:02:49,200 --> 00:02:55,080
so go to definition is F3 across all of

77
00:02:52,739 --> 00:02:57,480
these but it is usually more convenient

78
00:02:55,080 --> 00:02:59,400
to control click or command click on

79
00:02:57,480 --> 00:03:00,780
things unfortunately I can't show you

80
00:02:59,400 --> 00:03:03,000
that right now because I'm running this

81
00:03:00,780 --> 00:03:06,180
in a virtual machine and when I control

82
00:03:03,000 --> 00:03:09,120
click it intercepts my thing and treats

83
00:03:06,180 --> 00:03:11,580
it like it's a right click but you can

84
00:03:09,120 --> 00:03:13,379
see that when I hit the control key it

85
00:03:11,580 --> 00:03:15,659
turns this into a hyperlink looking

86
00:03:13,379 --> 00:03:17,640
thing and it changes my mouse cursor so

87
00:03:15,659 --> 00:03:19,620
under normal circumstances on Windows

88
00:03:17,640 --> 00:03:21,360
and Linux if you control clicked it

89
00:03:19,620 --> 00:03:22,680
would go through to the definition but

90
00:03:21,360 --> 00:03:26,159
for now I'm just going to go ahead and

91
00:03:22,680 --> 00:03:28,379
use the F3 so I used F3 and great I'm

92
00:03:26,159 --> 00:03:31,080
here and I can you know go read this

93
00:03:28,379 --> 00:03:33,959
code now but once I'm done reading the

94
00:03:31,080 --> 00:03:36,120
code well I need to find my way back to

95
00:03:33,959 --> 00:03:38,459
where I came from so the first way you

96
00:03:36,120 --> 00:03:40,920
can do that is just this forward and

97
00:03:38,459 --> 00:03:42,599
backwards arrows up here so that's

98
00:03:40,920 --> 00:03:45,360
useful but you can see when I click that

99
00:03:42,599 --> 00:03:47,819
it also tells me that the special

100
00:03:45,360 --> 00:03:50,819
keystrokes for that is I can hold alt

101
00:03:47,819 --> 00:03:53,280
and I can press the left Arrow to go

102
00:03:50,819 --> 00:03:56,220
backwards and I can hold alt and press

103
00:03:53,280 --> 00:03:58,080
the right arrow to go forwards so that's

104
00:03:56,220 --> 00:04:01,080
super useful if you've you know drilled

105
00:03:58,080 --> 00:04:03,420
down you know five levels deep into

106
00:04:01,080 --> 00:04:05,760
something and you've forgotten where you

107
00:04:03,420 --> 00:04:07,500
came from and you need to get your way

108
00:04:05,760 --> 00:04:09,780
back out of there you can just hold alt

109
00:04:07,500 --> 00:04:13,200
and go Arrow Arrow Arrow to work your

110
00:04:09,780 --> 00:04:15,420
way back up the call hierarchy

111
00:04:13,200 --> 00:04:18,120
so what other commands do we have here

112
00:04:15,420 --> 00:04:20,459
well we've seen search that's control h

113
00:04:18,120 --> 00:04:22,500
we've seen go to definition F3 or

114
00:04:20,459 --> 00:04:25,320
control click or command click

115
00:04:22,500 --> 00:04:26,759
now let's see find all references so

116
00:04:25,320 --> 00:04:28,620
this is something that you'll use

117
00:04:26,759 --> 00:04:30,540
whenever you're trying to say where is

118
00:04:28,620 --> 00:04:32,820
this function used in the entire code

119
00:04:30,540 --> 00:04:35,820
base or where is this variable used

120
00:04:32,820 --> 00:04:37,740
within a function so init Dev ring for

121
00:04:35,820 --> 00:04:39,660
instance I want to say where is this

122
00:04:37,740 --> 00:04:42,000
found in the entire code base that I'm

123
00:04:39,660 --> 00:04:43,919
looking at right here so to find all

124
00:04:42,000 --> 00:04:45,960
references to that function I would hit

125
00:04:43,919 --> 00:04:49,320
Ctrl shift G

126
00:04:45,960 --> 00:04:52,259
so I have clicked on that and I hit Ctrl

127
00:04:49,320 --> 00:04:54,960
shift G and then now down here in the

128
00:04:52,259 --> 00:04:57,419
search it shows me okay this is used

129
00:04:54,960 --> 00:04:59,940
right here and then it's basically being

130
00:04:57,419 --> 00:05:01,919
called and it's called here as well so

131
00:04:59,940 --> 00:05:03,540
for each of those I could go to it and

132
00:05:01,919 --> 00:05:05,460
then I would see that's a location that

133
00:05:03,540 --> 00:05:07,500
this is actually used and then I might

134
00:05:05,460 --> 00:05:10,080
say okay well what function is that in

135
00:05:07,500 --> 00:05:12,000
and where is that function used and I do

136
00:05:10,080 --> 00:05:14,100
the thing again and so I would see

137
00:05:12,000 --> 00:05:15,840
that's only used in one location and

138
00:05:14,100 --> 00:05:18,180
again you know we can keep doing this

139
00:05:15,840 --> 00:05:20,220
but instead of for instance seeing like

140
00:05:18,180 --> 00:05:22,320
where is something used and then just

141
00:05:20,220 --> 00:05:24,960
walking your way back one thing at a

142
00:05:22,320 --> 00:05:28,020
time another extremely useful thing is

143
00:05:24,960 --> 00:05:30,419
to open the call hierarchy and so that

144
00:05:28,020 --> 00:05:32,820
is going to be a backwards facing look

145
00:05:30,419 --> 00:05:34,919
at all of the functions that called into

146
00:05:32,820 --> 00:05:38,100
the function you're in right now so

147
00:05:34,919 --> 00:05:39,960
let's go back using our alt left Arrow

148
00:05:38,100 --> 00:05:42,539
to get back to that first function that

149
00:05:39,960 --> 00:05:44,940
we're in okay so init Dev ring I said

150
00:05:42,539 --> 00:05:47,039
that's one of the interesting functions

151
00:05:44,940 --> 00:05:49,500
for us to look at now let's see like

152
00:05:47,039 --> 00:05:52,199
what are all the various call paths that

153
00:05:49,500 --> 00:05:55,440
can actually reach this so to do that we

154
00:05:52,199 --> 00:05:58,039
do Ctrl alt h

155
00:05:55,440 --> 00:06:00,600
Control Alt h

156
00:05:58,039 --> 00:06:02,820
and this will bring up a separate type

157
00:06:00,600 --> 00:06:06,000
of tab which you can expand out it'll

158
00:06:02,820 --> 00:06:09,300
say init Dev ring is only called from

159
00:06:06,000 --> 00:06:13,020
load DSR if I expand that then I see

160
00:06:09,300 --> 00:06:15,360
that is only called from PVR dma reg's

161
00:06:13,020 --> 00:06:17,039
right if I expand that I can see that's

162
00:06:15,360 --> 00:06:19,199
called from some sort of function

163
00:06:17,039 --> 00:06:21,660
pointer usage in some sort of data

164
00:06:19,199 --> 00:06:23,819
structure so personally I usually use

165
00:06:21,660 --> 00:06:26,699
call hierarchies when I'm trying to

166
00:06:23,819 --> 00:06:29,340
understand if I think something is ACID

167
00:06:26,699 --> 00:06:31,380
I'm trying to back trace and say okay I

168
00:06:29,340 --> 00:06:33,000
think that this name is ACID but like

169
00:06:31,380 --> 00:06:35,039
where did that come from who called this

170
00:06:33,000 --> 00:06:37,380
and I say okay well that's called by

171
00:06:35,039 --> 00:06:39,419
this so which parameter was name okay

172
00:06:37,380 --> 00:06:41,400
well I've already lost it which one is

173
00:06:39,419 --> 00:06:44,340
it one two three it's the third

174
00:06:41,400 --> 00:06:46,319
parameter and I go one two three okay

175
00:06:44,340 --> 00:06:48,720
name nope that's a hard coded constant

176
00:06:46,319 --> 00:06:50,340
that is not asset so that's the kind of

177
00:06:48,720 --> 00:06:52,319
way that you can sort of walk your way

178
00:06:50,340 --> 00:06:53,699
backwards if you you know for instance

179
00:06:52,319 --> 00:06:55,020
let's say that you use the sort of

180
00:06:53,699 --> 00:06:57,780
heuristics you learn in vulnerabilities

181
00:06:55,020 --> 00:07:00,000
1001 you just you know grept for a mem

182
00:06:57,780 --> 00:07:03,360
card happy so you know how would we do

183
00:07:00,000 --> 00:07:05,039
that control h and then search for mem

184
00:07:03,360 --> 00:07:07,199
copy I don't actually know if there's

185
00:07:05,039 --> 00:07:10,380
any in this code base oh yes there are

186
00:07:07,199 --> 00:07:12,060
1000 of them great so if you found some

187
00:07:10,380 --> 00:07:13,680
memcpy and you were saying to yourself

188
00:07:12,060 --> 00:07:16,319
hey I want to know whether or not this

189
00:07:13,680 --> 00:07:19,380
is ACID well then you could just open

190
00:07:16,319 --> 00:07:22,620
the call hierarchy from here so that

191
00:07:19,380 --> 00:07:24,840
control alt H and then you would work

192
00:07:22,620 --> 00:07:27,360
your way backwards and say who called

193
00:07:24,840 --> 00:07:30,660
what with what parameters to ultimately

194
00:07:27,360 --> 00:07:32,639
feed in this thing and is that ACID so

195
00:07:30,660 --> 00:07:34,199
call hierarchy is super useful for back

196
00:07:32,639 --> 00:07:36,960
tracing ACID

197
00:07:34,199 --> 00:07:40,740
now let me go back and show you again

198
00:07:36,960 --> 00:07:43,740
one of those other versions of find all

199
00:07:40,740 --> 00:07:47,099
references so I previously used the

200
00:07:43,740 --> 00:07:49,400
final references on this function so now

201
00:07:47,099 --> 00:07:52,919
let's find all references Ctrl shift G

202
00:07:49,400 --> 00:07:56,039
and I used it on a function you can also

203
00:07:52,919 --> 00:07:58,979
use it on things like variables so I

204
00:07:56,039 --> 00:08:01,440
select that variable I do Ctrl shift G

205
00:07:58,979 --> 00:08:03,419
and then it shows me all of the usages

206
00:08:01,440 --> 00:08:05,520
of that variable here within that

207
00:08:03,419 --> 00:08:07,740
function so that can be useful for just

208
00:08:05,520 --> 00:08:09,539
a quick overview skim like if you're

209
00:08:07,740 --> 00:08:12,240
looking for assignment where the thing

210
00:08:09,539 --> 00:08:14,099
is assigned a value or where the thing

211
00:08:12,240 --> 00:08:16,800
is assigned to something else if you're

212
00:08:14,099 --> 00:08:18,960
trying to track ACID and you want to see

213
00:08:16,800 --> 00:08:20,879
okay what's going to be tainted by this

214
00:08:18,960 --> 00:08:23,220
particular variable then that's another

215
00:08:20,879 --> 00:08:25,560
way you can use the find all references

216
00:08:23,220 --> 00:08:27,660
so frequently you're going to use that

217
00:08:25,560 --> 00:08:29,400
just to kind of get a sense if you're in

218
00:08:27,660 --> 00:08:31,080
like giant functions like this is a

219
00:08:29,400 --> 00:08:32,399
relatively small function but if you're

220
00:08:31,080 --> 00:08:34,140
in a giant function you're just trying

221
00:08:32,399 --> 00:08:36,899
to see okay like how is this variable U

222
00:08:34,140 --> 00:08:39,240
used then the find all references is

223
00:08:36,899 --> 00:08:40,320
super useful for that and so we've

224
00:08:39,240 --> 00:08:42,240
looked through all the essential

225
00:08:40,320 --> 00:08:44,520
commands at this point except for the

226
00:08:42,240 --> 00:08:47,700
grep for selected text and this is

227
00:08:44,520 --> 00:08:50,220
really just a similar version to if you

228
00:08:47,700 --> 00:08:51,839
hit Ctrl H and then you did the file

229
00:08:50,220 --> 00:08:54,420
search and then you put some particular

230
00:08:51,839 --> 00:08:57,000
text in there except it gives you less

231
00:08:54,420 --> 00:08:59,880
steps so if you wanted to literally just

232
00:08:57,000 --> 00:09:02,580
select this and find this text you could

233
00:08:59,880 --> 00:09:05,040
do Ctrl H and you could paste that in

234
00:09:02,580 --> 00:09:06,300
and search for it like that but the

235
00:09:05,040 --> 00:09:08,040
faster version that you're going to want

236
00:09:06,300 --> 00:09:10,080
to use because you're probably going to

237
00:09:08,040 --> 00:09:14,160
be doing this all the time is you want

238
00:09:10,080 --> 00:09:16,560
to do Control Alt G so I just select

239
00:09:14,160 --> 00:09:19,500
some text and I say like I want to see

240
00:09:16,560 --> 00:09:22,200
this thing right here and I do Ctrl alt

241
00:09:19,500 --> 00:09:24,720
G and that is the equivalent of doing

242
00:09:22,200 --> 00:09:26,880
that search elsewhere here you can see

243
00:09:24,720 --> 00:09:29,519
that this was collapsed so I might do

244
00:09:26,880 --> 00:09:31,260
the collapse all expand all and then I

245
00:09:29,519 --> 00:09:33,660
can quickly see how this is used

246
00:09:31,260 --> 00:09:36,360
elsewhere okay so those are the

247
00:09:33,660 --> 00:09:38,339
essential commands and there's really no

248
00:09:36,360 --> 00:09:40,019
good way to unfortunately because of the

249
00:09:38,339 --> 00:09:41,760
overlapping of you know there's a lot of

250
00:09:40,019 --> 00:09:44,760
G's and there's a lot of H's here

251
00:09:41,760 --> 00:09:46,560
there's no real good way to do this

252
00:09:44,760 --> 00:09:48,839
except memorize it and I recommend just

253
00:09:46,560 --> 00:09:50,519
you know print this out and stick it on

254
00:09:48,839 --> 00:09:53,000
a piece of paper next to you so you can

255
00:09:50,519 --> 00:09:53,000
just look down

