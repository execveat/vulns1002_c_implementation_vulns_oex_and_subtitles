1
00:00:00,299 --> 00:00:05,279
welcome back folks to vulnerabilities

2
00:00:02,300 --> 00:00:08,040
1002 C-family software implementation

3
00:00:05,279 --> 00:00:09,720
vulnerabilities a nice generic name so

4
00:00:08,040 --> 00:00:11,940
let's continue where we left off from

5
00:00:09,720 --> 00:00:13,980
vulnerabilities 1001 because this class

6
00:00:11,940 --> 00:00:15,420
is not meant to stand alone this class

7
00:00:13,980 --> 00:00:17,760
fully expects that you've already

8
00:00:15,420 --> 00:00:19,320
completed vulnerabilities 1001 because

9
00:00:17,760 --> 00:00:21,359
we're just going to pick right back up

10
00:00:19,320 --> 00:00:22,980
and use all the same terminology and

11
00:00:21,359 --> 00:00:26,580
conventions that we used in that class

12
00:00:22,980 --> 00:00:29,340
but we do have a few new words of power

13
00:00:26,580 --> 00:00:31,199
the things that an attacker should concern

14
00:00:29,340 --> 00:00:33,120
themselves with because they potentially

15
00:00:31,199 --> 00:00:35,700
give an attacker an advantage things

16
00:00:33,120 --> 00:00:38,760
like uninitialized or initialized or

17
00:00:35,700 --> 00:00:41,579
uninitialized things like shared memory

18
00:00:38,760 --> 00:00:44,100
or the lack of mutual exclusion on

19
00:00:41,579 --> 00:00:47,040
shared memory that could be a problem

20
00:00:44,100 --> 00:00:50,160
things like reference counting that

21
00:00:47,040 --> 00:00:53,700
could lead to retain release leading to

22
00:00:50,160 --> 00:00:55,980
frees that are premature and also the

23
00:00:53,700 --> 00:00:57,780
interpretation of data as multiple types

24
00:00:55,980 --> 00:00:59,820
all of these things are going to be

25
00:00:57,780 --> 00:01:02,219
Words of Power things that should set

26
00:00:59,820 --> 00:01:04,440
off your sploity sense because as with

27
00:01:02,219 --> 00:01:07,320
vulnerabilities 1001 this class is once

28
00:01:04,440 --> 00:01:09,119
again predicated on the idea that if you

29
00:01:07,320 --> 00:01:11,159
continue to read about vulnerabilities

30
00:01:09,119 --> 00:01:13,200
the more that you see the better you

31
00:01:11,159 --> 00:01:14,820
develop your pattern recognition and

32
00:01:13,200 --> 00:01:17,340
eventually you too can become a

33
00:01:14,820 --> 00:01:19,380
vulnerability Hunter with a good sploity

34
00:01:17,340 --> 00:01:21,479
sense and as always we want you to

35
00:01:19,380 --> 00:01:26,299
program paranoid because it's not really

36
00:01:21,479 --> 00:01:26,299
paranoia if they are out to get you

