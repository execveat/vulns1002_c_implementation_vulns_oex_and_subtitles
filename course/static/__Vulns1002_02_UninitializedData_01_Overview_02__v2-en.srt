1
00:00:00,780 --> 00:00:05,279
I want to very briefly go over how an

2
00:00:03,600 --> 00:00:06,779
attacker would try to exploit the UDA

3
00:00:05,279 --> 00:00:08,220
vulnerabilities in the hopes that it

4
00:00:06,779 --> 00:00:09,780
gives you a little bit more insight

5
00:00:08,220 --> 00:00:11,400
about how the vulnerabilities occur in

6
00:00:09,780 --> 00:00:13,980
the first place so I'm going to

7
00:00:11,400 --> 00:00:16,080
introduce stack grooming or stack feng

8
00:00:13,980 --> 00:00:17,820
shui to contrast it against a heap

9
00:00:16,080 --> 00:00:20,340
grooming that you've already seen in the

10
00:00:17,820 --> 00:00:22,320
previous class and so in this case the

11
00:00:20,340 --> 00:00:24,180
attacker knows that there exists a path

12
00:00:22,320 --> 00:00:26,760
in the code where down some particular

13
00:00:24,180 --> 00:00:29,519
function calls trace it's going to

14
00:00:26,760 --> 00:00:31,980
ultimately use uninitialized data so the

15
00:00:29,519 --> 00:00:34,320
attacker's goal is to follow some other

16
00:00:31,980 --> 00:00:36,780
control flow path that allows them to

17
00:00:34,320 --> 00:00:39,120
set attacker-controlled data on the stack

18
00:00:36,780 --> 00:00:42,420
at the exact offset that they know will

19
00:00:39,120 --> 00:00:44,100
be used for UDA access later so if we

20
00:00:42,420 --> 00:00:45,780
can imagine that for instance there was

21
00:00:44,100 --> 00:00:48,059
one control flow path main calls

22
00:00:45,780 --> 00:00:51,059
func5() calls func6() calls

23
00:00:48,059 --> 00:00:53,640
UDA_get() and this UDA_get() reads from the

24
00:00:51,059 --> 00:00:55,739
stack at this particular offset and they

25
00:00:53,640 --> 00:00:57,960
read uninitialized data now the

26
00:00:55,739 --> 00:01:00,120
attacker's target is to make sure they

27
00:00:57,960 --> 00:01:03,120
can set that exact location on the stack

28
00:01:00,120 --> 00:01:04,860
to ACID so let's imagine that you know

29
00:01:03,120 --> 00:01:06,600
they're calling main and main calls

30
00:01:04,860 --> 00:01:08,640
f1() and that calls f2()

31
00:01:06,600 --> 00:01:10,380
and that calls f3() and then

32
00:01:08,640 --> 00:01:12,240
f3() returns and then f2()

33
00:01:10,380 --> 00:01:15,240
returns and then f4() is

34
00:01:12,240 --> 00:01:17,640
called and then now the attacker sees

35
00:01:15,240 --> 00:01:19,860
some function that they can feed ACID

36
00:01:17,640 --> 00:01:22,860
into that will put their ACID on the

37
00:01:19,860 --> 00:01:24,479
stack exactly at the target location for

38
00:01:22,860 --> 00:01:26,880
UDA access later

39
00:01:24,479 --> 00:01:28,860
then that function the setter() function

40
00:01:26,880 --> 00:01:31,320
will return but that will still be set

41
00:01:28,860 --> 00:01:33,780
on the stack and then f4() returns and

42
00:01:31,320 --> 00:01:36,659
then f1() returns and then f5() is called

43
00:01:33,780 --> 00:01:39,780
and then f6() is called and then the

44
00:01:36,659 --> 00:01:42,000
UDA_get() function is accessing uninitialized

45
00:01:39,780 --> 00:01:44,520
data that the attacker has successfully

46
00:01:42,000 --> 00:01:46,979
filled in with ACID and that's going to

47
00:01:44,520 --> 00:01:48,900
go kaboom so that's what we saw in the

48
00:01:46,979 --> 00:01:50,939
trivial example but this is just you

49
00:01:48,900 --> 00:01:52,619
know to say generically the more

50
00:01:50,939 --> 00:01:54,899
complicated the code it's all about you

51
00:01:52,619 --> 00:01:56,939
know how deep in the stack frame does

52
00:01:54,899 --> 00:01:59,100
the attacker have to go to set the data

53
00:01:56,939 --> 00:02:01,860
at the right location for the eventual

54
00:01:59,100 --> 00:02:03,720
stack frame that has an UDA access now I

55
00:02:01,860 --> 00:02:05,880
want to quickly contrast heap grooming

56
00:02:03,720 --> 00:02:08,520
and heap feng shui in the context of the

57
00:02:05,880 --> 00:02:10,860
original heap overflows versus

58
00:02:08,520 --> 00:02:12,720
UDA access so in the original thing the

59
00:02:10,860 --> 00:02:13,980
attacker would set these yellow values

60
00:02:12,720 --> 00:02:15,900
there were things that the attacker

61
00:02:13,980 --> 00:02:18,300
didn't necessarily control but they were

62
00:02:15,900 --> 00:02:21,000
just trying to influence the filling in

63
00:02:18,300 --> 00:02:23,340
of the heap then they would cause a

64
00:02:21,000 --> 00:02:25,739
selective deallocation to create a gap

65
00:02:23,340 --> 00:02:27,900
they would let the victim allocation

66
00:02:25,739 --> 00:02:30,120
fill in the gap and this is the data

67
00:02:27,900 --> 00:02:32,700
what would subsequently be overwritten

68
00:02:30,120 --> 00:02:35,280
then they cause an adjacent deallocation

69
00:02:32,700 --> 00:02:36,840
of some vulnerable buffer that they know

70
00:02:35,280 --> 00:02:38,400
they can overflow and when they

71
00:02:36,840 --> 00:02:40,440
eventually overflow the vulnerable

72
00:02:38,400 --> 00:02:42,900
buffer it will overflow into the victim

73
00:02:40,440 --> 00:02:45,840
data which will subsequently be using

74
00:02:42,900 --> 00:02:48,060
ACID instead of the clean data 

75
00:02:45,120 --> 00:02:49,799
so in contrast when you're doing heap

76
00:02:47,700 --> 00:02:51,780
grooming in the context of UDA

77
00:02:49,799 --> 00:02:53,159
vulnerabilities the attacker knows that

78
00:02:51,780 --> 00:02:55,200
eventually there's going to be some

79
00:02:53,159 --> 00:02:57,480
access to uninitialized data on the heap

80
00:02:55,200 --> 00:02:59,579
so they would instead try to fill in all

81
00:02:57,480 --> 00:03:02,280
attacker-controlled value they could

82
00:02:59,579 --> 00:03:05,820
cause some selective deallocations to

83
00:03:02,280 --> 00:03:07,799
once again let the victim data fall in

84
00:03:05,820 --> 00:03:10,019
there so the selective deallocation will

85
00:03:07,799 --> 00:03:11,579
of course start out as uninitialized but

86
00:03:10,019 --> 00:03:14,820
we know that behind the scenes it's

87
00:03:11,579 --> 00:03:16,439
really still ACID values so if the

88
00:03:14,820 --> 00:03:18,960
location right here is what's going to

89
00:03:16,439 --> 00:03:21,120
be used as uninitialized later the

90
00:03:18,960 --> 00:03:22,500
legitimate code may initialize some of

91
00:03:21,120 --> 00:03:24,480
the data but then it forgot to

92
00:03:22,500 --> 00:03:27,000
initialize all of it and so it's going

93
00:03:24,480 --> 00:03:28,920
to access this bit right here and behind

94
00:03:27,000 --> 00:03:31,500
the scenes that bit right there still

95
00:03:28,920 --> 00:03:33,840
has whatever ACID value the attacker had

96
00:03:31,500 --> 00:03:37,019
filled in on the heap and therefore that

97
00:03:33,840 --> 00:03:39,060
is going to be using malicious values

98
00:03:37,019 --> 00:03:41,159
and then when we go back to the concept

99
00:03:39,060 --> 00:03:43,500
of heap sprays we said that in a lot of

100
00:03:41,159 --> 00:03:45,960
cases for doing keep overflows heap

101
00:03:43,500 --> 00:03:48,480
sprays were not necessarily the best

102
00:03:45,960 --> 00:03:51,120
strategy but here they're a really good

103
00:03:48,480 --> 00:03:53,820
strategy you can just fill in all sorts

104
00:03:51,120 --> 00:03:55,680
of ACID on the heap and free all of that

105
00:03:53,820 --> 00:03:57,840
ACID on the heap and then eventually

106
00:03:55,680 --> 00:04:00,299
when the victim is trying to use

107
00:03:57,840 --> 00:04:02,160
something anywhere on the heap it's

108
00:04:00,299 --> 00:04:04,560
going to be ACID so if there's

109
00:04:02,160 --> 00:04:06,840
uninitialized access of some allocation

110
00:04:04,560 --> 00:04:09,120
from anywhere if the attacker filled in

111
00:04:06,840 --> 00:04:11,400
all those allocations with ACID then

112
00:04:09,120 --> 00:04:12,479
it'll be ACID use for an UDA

113
00:04:11,400 --> 00:04:14,580
vulnerability

114
00:04:12,479 --> 00:04:17,660
so enough of the background let's go see

115
00:04:14,580 --> 00:04:17,660
some real vulnerabilities

