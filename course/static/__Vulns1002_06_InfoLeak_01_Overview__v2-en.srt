1
00:00:00,359 --> 00:00:05,100
let's talk about information disclosure

2
00:00:02,580 --> 00:00:07,500
or info leak vulnerabilities what are

3
00:00:05,100 --> 00:00:09,480
they well they're the induced disclosure

4
00:00:07,500 --> 00:00:11,519
of information that is meant to remain

5
00:00:09,480 --> 00:00:14,040
confidential and of course in the

6
00:00:11,519 --> 00:00:16,320
context of this class they are induced

7
00:00:14,040 --> 00:00:18,480
through ACID but of course that doesn't

8
00:00:16,320 --> 00:00:20,640
always have to be the case so why do

9
00:00:18,480 --> 00:00:22,980
they matter well they can be used to

10
00:00:20,640 --> 00:00:25,740
defeat exploit mitigations that depend

11
00:00:22,980 --> 00:00:28,380
on randomization which typically depends

12
00:00:25,740 --> 00:00:30,359
on some sort of randomized secret so

13
00:00:28,380 --> 00:00:32,460
address-based layout randomization for

14
00:00:30,359 --> 00:00:34,020
instance depends on the attacker not

15
00:00:32,460 --> 00:00:35,880
knowing the layout of memory and

16
00:00:34,020 --> 00:00:38,340
stack canaries are just random values

17
00:00:35,880 --> 00:00:40,260
stuck onto the stack so if an attacker

18
00:00:38,340 --> 00:00:43,079
can bypass these exploit mitigations

19
00:00:40,260 --> 00:00:45,239
then they can perhaps achieve full

20
00:00:43,079 --> 00:00:47,700
exploit chains and achieve their final

21
00:00:45,239 --> 00:00:49,860
goals additionally info leaks can matter

22
00:00:47,700 --> 00:00:50,879
not for the purposes of the typical sort

23
00:00:49,860 --> 00:00:52,620
of memory corruption type

24
00:00:50,879 --> 00:00:55,140
vulnerabilities that we've been focusing

25
00:00:52,620 --> 00:00:57,840
on in these classes but just disclosing

26
00:00:55,140 --> 00:01:00,300
secrets in general can be bad because if

27
00:00:57,840 --> 00:01:02,160
things like cryptographic keys documents

28
00:01:00,300 --> 00:01:04,440
that are stored in memory passwords

29
00:01:02,160 --> 00:01:07,320
keystrokes if any of those kind of

30
00:01:04,440 --> 00:01:09,240
things get leaked out of memory then the

31
00:01:07,320 --> 00:01:11,700
defender is potentially having a bad day

32
00:01:09,240 --> 00:01:13,439
right you don't want your passwords to

33
00:01:11,700 --> 00:01:17,159
be leaked out of memory once again the

34
00:01:13,439 --> 00:01:19,020
US guy no like lot word chunk so instead

35
00:01:17,159 --> 00:01:21,240
of information disclosure we're going to

36
00:01:19,020 --> 00:01:22,140
go with the much more common info leaks

37
00:01:21,240 --> 00:01:23,880
term

38
00:01:22,140 --> 00:01:26,640
now returning again to why they matter

39
00:01:23,880 --> 00:01:29,820
let's talk about the exploit enabler so

40
00:01:26,640 --> 00:01:33,479
we had seen in a past example so this

41
00:01:29,820 --> 00:01:36,000
one right here CVE-2020-17087 covered that

42
00:01:33,479 --> 00:01:38,340
in vulnerabilities 1001 and we saw this

43
00:01:36,000 --> 00:01:40,860
diagram from a Google Project Zero blog

44
00:01:38,340 --> 00:01:43,380
post that talked about how the attacker

45
00:01:40,860 --> 00:01:45,299
achieved their goals via exploit chains

46
00:01:43,380 --> 00:01:46,979
and so they would infect a website

47
00:01:45,299 --> 00:01:49,200
people go to the website they would

48
00:01:46,979 --> 00:01:50,880
break into the browser whether Safari or

49
00:01:49,200 --> 00:01:52,560
Google Chrome and then depending on

50
00:01:50,880 --> 00:01:54,840
whether or not they land on for instance

51
00:01:52,560 --> 00:01:56,100
iOS versus Windows they would have

52
00:01:54,840 --> 00:01:58,619
different privilege escalation

53
00:01:56,100 --> 00:02:00,060
vulnerabilities in the case of iOS they

54
00:01:58,619 --> 00:02:02,460
said before they could privilege

55
00:02:00,060 --> 00:02:04,020
escalate they had to use an info leak to

56
00:02:02,460 --> 00:02:06,540
defeat to the address space layout

57
00:02:04,020 --> 00:02:08,819
randomization and indeed there were

58
00:02:06,540 --> 00:02:10,319
other blog posts by Google Project Zero

59
00:02:08,819 --> 00:02:13,800
that looked at all sorts of different

60
00:02:10,319 --> 00:02:16,020
exploit chains for iOS across time how

61
00:02:13,800 --> 00:02:19,200
long they lasted and which operating

62
00:02:16,020 --> 00:02:21,239
systems and phones they affected and for

63
00:02:19,200 --> 00:02:22,680
every single one of these exploit chains

64
00:02:21,239 --> 00:02:25,379
it actually required an information

65
00:02:22,680 --> 00:02:27,900
disclosure as well but it's not just iOS

66
00:02:25,379 --> 00:02:30,599
it's any operating system and info leak

67
00:02:27,900 --> 00:02:32,580
may be an integral portion of a exploit

68
00:02:30,599 --> 00:02:34,800
chain to achieve the final goals of

69
00:02:32,580 --> 00:02:36,840
privilege escalation so that's why info

70
00:02:34,800 --> 00:02:38,819
leaks matter for exploit mitigation

71
00:02:36,840 --> 00:02:41,580
bypass now let's return again to the

72
00:02:38,819 --> 00:02:44,099
idea of info leaks just allowing to

73
00:02:41,580 --> 00:02:45,900
steal contents from memory so there's a

74
00:02:44,099 --> 00:02:48,420
very famous vulnerability named heart

75
00:02:45,900 --> 00:02:50,760
bleed and it had this particular logo

76
00:02:48,420 --> 00:02:53,340
and this particular website and this was

77
00:02:50,760 --> 00:02:57,420
a bug in the handling of heartbeat

78
00:02:53,340 --> 00:02:59,280
packets in OpenSSL library and this

79
00:02:57,420 --> 00:03:02,220
particular bug allowed for remote

80
00:02:59,280 --> 00:03:05,760
disclosure over the internet of things

81
00:03:02,220 --> 00:03:08,760
like private keys from a server that's

82
00:03:05,760 --> 00:03:11,280
running open in SSL so it could be a VPN

83
00:03:08,760 --> 00:03:13,280
server could be a web server but if it

84
00:03:11,280 --> 00:03:15,239
was using this OpenSSL library

85
00:03:13,280 --> 00:03:17,700
essentially this vulnerability would

86
00:03:15,239 --> 00:03:20,459
allow an attacker to reach out and steal

87
00:03:17,700 --> 00:03:22,200
memory from that machine so that was

88
00:03:20,459 --> 00:03:24,480
super bad for an attacker to be able to

89
00:03:22,200 --> 00:03:26,280
go out and steal a private key from for

90
00:03:24,480 --> 00:03:28,140
instance a web server because then they

91
00:03:26,280 --> 00:03:30,420
could impersonate that website and

92
00:03:28,140 --> 00:03:32,819
browser lock icons would all show that

93
00:03:30,420 --> 00:03:34,319
everything was secure I will also point

94
00:03:32,819 --> 00:03:36,360
out that this Heartbleed

95
00:03:34,319 --> 00:03:38,940
vulnerability started the trend of

96
00:03:36,360 --> 00:03:40,920
giving logos and dedicated websites to

97
00:03:38,940 --> 00:03:43,319
vulnerabilities and things had been

98
00:03:40,920 --> 00:03:45,000
named before this but they you know

99
00:03:43,319 --> 00:03:46,799
released this nice little logo and it

100
00:03:45,000 --> 00:03:48,840
really took off and in this particular

101
00:03:46,799 --> 00:03:52,019
case I think it was appropriate because

102
00:03:48,840 --> 00:03:53,400
this was a very very bad bug but then a

103
00:03:52,019 --> 00:03:55,620
bunch of other people jumped on this

104
00:03:53,400 --> 00:03:57,360
bandwagon and they just wanted their own

105
00:03:55,620 --> 00:03:59,459
bug and website because they saw how

106
00:03:57,360 --> 00:04:01,799
much press attention this had gotten and

107
00:03:59,459 --> 00:04:04,280
so not everything with a logo and a

108
00:04:01,799 --> 00:04:08,340
website is necessarily an important bug

109
00:04:04,280 --> 00:04:09,959
sometimes it's just oh VC funded

110
00:04:08,340 --> 00:04:11,340
startups that are trying to get

111
00:04:09,959 --> 00:04:14,280
attention

112
00:04:11,340 --> 00:04:16,260
well info leaks are an important part of

113
00:04:14,280 --> 00:04:18,299
a perfectly balanced breakfast perfectly

114
00:04:16,260 --> 00:04:21,000
balanced as all things should be

115
00:04:18,299 --> 00:04:23,820
so info leaks along with ACID pointers

116
00:04:21,000 --> 00:04:27,780
and stack or heap overflows and integer

117
00:04:23,820 --> 00:04:29,940
underflows sign sizes well Thanos allows

118
00:04:27,780 --> 00:04:32,040
you to just write what where and the

119
00:04:29,940 --> 00:04:35,460
analogy here is that in this perfectly

120
00:04:32,040 --> 00:04:37,259
balanced breakfast your code is toast

121
00:04:35,460 --> 00:04:39,060
and so if you happen to notice that all

122
00:04:37,259 --> 00:04:41,460
of these look like vulnerability 1001

123
00:04:39,060 --> 00:04:43,620
types of problems that's because I made

124
00:04:41,460 --> 00:04:46,380
these slides last year and we just

125
00:04:43,620 --> 00:04:47,940
pushed info leaks back to 1002. so in

126
00:04:46,380 --> 00:04:50,340
full leak vulnerabilities where do they

127
00:04:47,940 --> 00:04:52,800
come from what do they want from us who

128
00:04:50,340 --> 00:04:55,320
do they think they are

129
00:04:52,800 --> 00:04:57,380
that's your Weird Al deep cut for the

130
00:04:55,320 --> 00:05:00,660
day slime creatures from outer space

131
00:04:57,380 --> 00:05:03,180
1995. dare to be stupid album

132
00:05:00,660 --> 00:05:04,919
so there's different types of info leaks

133
00:05:03,180 --> 00:05:07,580
we can consider microarchitectural

134
00:05:04,919 --> 00:05:09,600
intrinsic and manufactured now

135
00:05:07,580 --> 00:05:12,060
microarchitectural vulnerabilities are

136
00:05:09,600 --> 00:05:14,460
out of scope for this class just like

137
00:05:12,060 --> 00:05:16,440
with race conditions we put some of them

138
00:05:14,460 --> 00:05:18,419
out of scope because the recommendations

139
00:05:16,440 --> 00:05:20,100
wouldn't really follow along or be

140
00:05:18,419 --> 00:05:22,080
related to anything else in the class

141
00:05:20,100 --> 00:05:23,580
similarly microarchitectural

142
00:05:22,080 --> 00:05:25,259
vulnerabilities they're absolutely

143
00:05:23,580 --> 00:05:28,259
important in full leak vulnerabilities

144
00:05:25,259 --> 00:05:29,699
but the guidance for how to mitigate

145
00:05:28,259 --> 00:05:31,919
them is going to be completely and

146
00:05:29,699 --> 00:05:33,600
wildly different depending on the

147
00:05:31,919 --> 00:05:35,759
particular architecture depending on the

148
00:05:33,600 --> 00:05:37,500
particular vulnerability so basically

149
00:05:35,759 --> 00:05:39,180
the guidance for these are not going to

150
00:05:37,500 --> 00:05:41,100
align with any of the other guidance in

151
00:05:39,180 --> 00:05:42,539
this class so out of scope for now maybe

152
00:05:41,100 --> 00:05:44,940
in a future class

153
00:05:42,539 --> 00:05:47,940
but these so-called micro architectural

154
00:05:44,940 --> 00:05:49,800
side channel attacks exploit features of

155
00:05:47,940 --> 00:05:53,100
the microarchitecture the underlying

156
00:05:49,800 --> 00:05:55,020
architecture of how CPUs work so in the

157
00:05:53,100 --> 00:05:57,360
case of these famous things Spectre and

158
00:05:55,020 --> 00:06:00,060
Meltdown there were particular ways that

159
00:05:57,360 --> 00:06:03,300
the Intel CPU handled things like

160
00:06:00,060 --> 00:06:05,639
speculative execution for Spectre and so

161
00:06:03,300 --> 00:06:08,400
you had particular branches and the CPUs

162
00:06:05,639 --> 00:06:09,660
assembly coming along and it says oh I

163
00:06:08,400 --> 00:06:11,400
think that I'm going to take this branch

164
00:06:09,660 --> 00:06:13,800
let me go ahead and execute a few of

165
00:06:11,400 --> 00:06:15,720
those assembly instructions before I

166
00:06:13,800 --> 00:06:17,039
actually get there and like just put

167
00:06:15,720 --> 00:06:19,080
them in the pipeline and get them going

168
00:06:17,039 --> 00:06:21,020
but that allowed ultimately for an

169
00:06:19,080 --> 00:06:23,580
attacker to leak information

170
00:06:21,020 --> 00:06:26,060
based on the things going down paths

171
00:06:23,580 --> 00:06:28,919
based on side effects or side channel

172
00:06:26,060 --> 00:06:31,979
analysis of things going down paths that

173
00:06:28,919 --> 00:06:33,780
were never actually properly executed so

174
00:06:31,979 --> 00:06:36,300
anyways microarchitectural attacks

175
00:06:33,780 --> 00:06:38,699
jumped on the named logo bandwagon big

176
00:06:36,300 --> 00:06:40,500
time and yes these were generally

177
00:06:38,699 --> 00:06:43,020
important but they're sort of

178
00:06:40,500 --> 00:06:44,880
diminishing importance and diminishing

179
00:06:43,020 --> 00:06:47,759
relevance things but still trying to

180
00:06:44,880 --> 00:06:50,340
stay on that bandwagon and get attention

181
00:06:47,759 --> 00:06:52,380
now intrinsic info leak vulnerabilities

182
00:06:50,340 --> 00:06:54,240
are things that actually just exist

183
00:06:52,380 --> 00:06:56,160
fundamentally in the code and so this is

184
00:06:54,240 --> 00:06:58,020
more like the kind of stuff that we've

185
00:06:56,160 --> 00:07:00,780
seen elsewhere in the class so for

186
00:06:58,020 --> 00:07:03,660
instance you can have stack or heap not

187
00:07:00,780 --> 00:07:06,180
overflow but over-read and so these will

188
00:07:03,660 --> 00:07:08,819
be caused by the same root causes as

189
00:07:06,180 --> 00:07:11,100
linear heap overflows and and stack

190
00:07:08,819 --> 00:07:14,039
overflows but instead of writes they're

191
00:07:11,100 --> 00:07:16,800
going to involve reads so for instance

192
00:07:14,039 --> 00:07:18,479
if we had a memcpy and kernelspace in

193
00:07:16,800 --> 00:07:20,160
userspace and it's going to memcpy

194
00:07:18,479 --> 00:07:22,860
from kernelspace out to userspace

195
00:07:20,160 --> 00:07:24,599
let's say that the kernel heap has some

196
00:07:22,860 --> 00:07:27,180
stuff that has all been initialized on

197
00:07:24,599 --> 00:07:28,800
the heap and then there's a memcpy but

198
00:07:27,180 --> 00:07:30,720
there's an attacker-controlled length

199
00:07:28,800 --> 00:07:33,900
and although it only wanted to pass back

200
00:07:30,720 --> 00:07:36,599
four times eight bytes instead 64 bytes

201
00:07:33,900 --> 00:07:38,520
are going to be passed back and if the

202
00:07:36,599 --> 00:07:40,560
source was right here and the dest is

203
00:07:38,520 --> 00:07:44,160
right here in userspace then this will

204
00:07:40,560 --> 00:07:46,020
lead to an overcopy and so this could

205
00:07:44,160 --> 00:07:48,300
potentially have information that's

206
00:07:46,020 --> 00:07:50,039
being disclosed that is advantageous to

207
00:07:48,300 --> 00:07:51,360
the attacker for instance kernel

208
00:07:50,039 --> 00:07:53,759
function pointers which on some

209
00:07:51,360 --> 00:07:55,800
operating systems if you know the data

210
00:07:53,759 --> 00:07:58,380
that's going to be copied and you get a

211
00:07:55,800 --> 00:08:00,180
function pointer back you may be able to

212
00:07:58,380 --> 00:08:02,280
just automatically calculate the

213
00:08:00,180 --> 00:08:04,919
displacement that the kernel has been

214
00:08:02,280 --> 00:08:06,780
located in memory where it's been you

215
00:08:04,919 --> 00:08:08,280
know randomized and once you have that

216
00:08:06,780 --> 00:08:10,080
displacement then you will know where

217
00:08:08,280 --> 00:08:11,699
all sorts of other kernel functions can

218
00:08:10,080 --> 00:08:13,560
be found which you might use for things

219
00:08:11,699 --> 00:08:14,759
like return oriented programming against

220
00:08:13,560 --> 00:08:16,800
the kernel

221
00:08:14,759 --> 00:08:18,780
all right other things like out of bound

222
00:08:16,800 --> 00:08:21,300
reads instead of out of bound writes

223
00:08:18,780 --> 00:08:23,220
well when we were dealing with ACID

224
00:08:21,300 --> 00:08:24,960
offsets and indices being used for

225
00:08:23,220 --> 00:08:26,819
out-of-bound rights you could have the

226
00:08:24,960 --> 00:08:29,819
exact same thing occurring for a read

227
00:08:26,819 --> 00:08:31,620
and not every in out of bound read is

228
00:08:29,819 --> 00:08:33,479
necessarily going to be an info leak you

229
00:08:31,620 --> 00:08:35,940
can absolutely have something go out of

230
00:08:33,479 --> 00:08:37,320
bounds but not feed the data back to an

231
00:08:35,940 --> 00:08:39,599
attacker so it wouldn't actually be

232
00:08:37,320 --> 00:08:41,459
leaking anything but for this class

233
00:08:39,599 --> 00:08:43,919
we're going to specifically be looking

234
00:08:41,459 --> 00:08:46,560
for situations where it will be an info

235
00:08:43,919 --> 00:08:48,959
leak so let's imagine that we had an

236
00:08:46,560 --> 00:08:51,480
index of 5 that is attacker-controlled

237
00:08:48,959 --> 00:08:53,880
and then this in array is pointing in

238
00:08:51,480 --> 00:08:55,860
kernelspace in the heap and it will say

239
00:08:53,880 --> 00:08:59,040
that the out is going to point out to

240
00:08:55,860 --> 00:09:02,459
userspace this index 5 is actually

241
00:08:59,040 --> 00:09:06,240
indexing past the end of that particular

242
00:09:02,459 --> 00:09:09,600
array and so subsequently the write from

243
00:09:06,240 --> 00:09:12,000
in to out will grab some information and

244
00:09:09,600 --> 00:09:13,860
it'll be stuck into userspace and I

245
00:09:12,000 --> 00:09:16,019
know I'm showing here that this is

246
00:09:13,860 --> 00:09:17,339
uninitialized data past the bounds of

247
00:09:16,019 --> 00:09:18,660
the array it doesn't necessarily have to

248
00:09:17,339 --> 00:09:21,300
be uninitialized data it could be

249
00:09:18,660 --> 00:09:23,760
initialized data from some other area of

250
00:09:21,300 --> 00:09:25,380
memory but the important thing is just

251
00:09:23,760 --> 00:09:27,480
you know if there's useful information

252
00:09:25,380 --> 00:09:29,160
like known function pointers the

253
00:09:27,480 --> 00:09:31,800
attacker can use that to de-randomize

254
00:09:29,160 --> 00:09:34,860
and bypass exploit mitigations

255
00:09:31,800 --> 00:09:36,899
then there's UDA uninitialized data

256
00:09:34,860 --> 00:09:38,519
access vulnerabilities so again when you

257
00:09:36,899 --> 00:09:39,959
have copies across these boundaries

258
00:09:38,519 --> 00:09:41,399
potentially you know we're going to just

259
00:09:39,959 --> 00:09:44,160
keep going with the address-based layout

260
00:09:41,399 --> 00:09:46,680
randomization leakage so let's say that

261
00:09:44,160 --> 00:09:49,800
some data was initialized on the heap

262
00:09:46,680 --> 00:09:52,279
and then it was freed on the heap and

263
00:09:49,800 --> 00:09:55,019
then we had a partial initialization

264
00:09:52,279 --> 00:09:57,540
which subsequently meant that there

265
00:09:55,019 --> 00:10:01,140
would be some leftover data left in

266
00:09:57,540 --> 00:10:03,899
between these fields of the struct then

267
00:10:01,140 --> 00:10:07,260
if this data was copied from kernel

268
00:10:03,899 --> 00:10:09,600
space to userspace that uninitialized

269
00:10:07,260 --> 00:10:11,399
area right there is what actually would

270
00:10:09,600 --> 00:10:13,140
be readable in userspace and

271
00:10:11,399 --> 00:10:15,240
subsequently disclose some information

272
00:10:13,140 --> 00:10:16,680
and once again things like function

273
00:10:15,240 --> 00:10:19,740
pointers could be useful for

274
00:10:16,680 --> 00:10:21,540
de-randomization all right so these sort

275
00:10:19,740 --> 00:10:23,279
of info leaks intrinsic info leaks can

276
00:10:21,540 --> 00:10:25,740
happen for all sorts of other reasons

277
00:10:23,279 --> 00:10:27,360
just wanted to show some examples of how

278
00:10:25,740 --> 00:10:28,980
they could occur with the bug types that

279
00:10:27,360 --> 00:10:31,140
we've already learned about you'll see

280
00:10:28,980 --> 00:10:33,240
more of that in the real examples then

281
00:10:31,140 --> 00:10:35,580
of course we have manufactured info

282
00:10:33,240 --> 00:10:37,440
leaks and if you've been watching all of

283
00:10:35,580 --> 00:10:39,779
the content rather than just a subset

284
00:10:37,440 --> 00:10:42,000
you will have already seen manufactured

285
00:10:39,779 --> 00:10:43,820
info leaks in the context of some

286
00:10:42,000 --> 00:10:46,500
particular exploit explanation

287
00:10:43,820 --> 00:10:48,300
in vulnerabilities 1001 there was the SMBGhost

288
00:10:46,500 --> 00:10:51,720
vulnerability where they created

289
00:10:48,300 --> 00:10:55,380
an info leak and in 1002 there was this

290
00:10:51,720 --> 00:10:58,079
vulnerability CVE-2021-1732 and it's variant

291
00:10:55,380 --> 00:11:00,060
vulnerability where the attacker could

292
00:10:58,079 --> 00:11:02,160
leak information after they had

293
00:11:00,060 --> 00:11:03,899
successfully corrupted memory and caused

294
00:11:02,160 --> 00:11:06,600
a type confusion

295
00:11:03,899 --> 00:11:08,399
so you know if we think about overflows

296
00:11:06,600 --> 00:11:10,380
and out of bound writes you can imagine

297
00:11:08,399 --> 00:11:12,000
that the attacker will use the write

298
00:11:10,380 --> 00:11:13,980
primitive whatever it is whether it's

299
00:11:12,000 --> 00:11:16,079
stack heap buffer overflow whether it's

300
00:11:13,980 --> 00:11:18,779
out of bound write if they can overwrite

301
00:11:16,079 --> 00:11:21,839
a data pointer creating an ACID pointer

302
00:11:18,779 --> 00:11:24,060
then if they execute some existing

303
00:11:21,839 --> 00:11:26,220
legitimate code that reads from that

304
00:11:24,060 --> 00:11:27,839
pointer well they can point it anywhere

305
00:11:26,220 --> 00:11:30,540
they want and so they can leak

306
00:11:27,839 --> 00:11:32,820
information from anywhere they want

307
00:11:30,540 --> 00:11:34,260
in the context of type confusions you

308
00:11:32,820 --> 00:11:36,839
could have situations where different

309
00:11:34,260 --> 00:11:38,579
types have different sizes and so if you

310
00:11:36,839 --> 00:11:41,760
imagine that you know type A was 30

311
00:11:38,579 --> 00:11:43,680
bytes and type B is 256 bytes and maybe

312
00:11:41,760 --> 00:11:46,079
there is some sort of type copying

313
00:11:43,680 --> 00:11:48,420
function or constructor that's being

314
00:11:46,079 --> 00:11:50,760
used based on a particular type if

315
00:11:48,420 --> 00:11:54,779
something is confused into thinking A is

316
00:11:50,760 --> 00:11:57,120
a B it may copy 256 bytes from A instead

317
00:11:54,779 --> 00:11:58,620
of 30. so finding manufactured info

318
00:11:57,120 --> 00:12:01,320
leaks is not the goal of this class

319
00:11:58,620 --> 00:12:03,300
finding intrinsic ones is because at the

320
00:12:01,320 --> 00:12:05,459
end of the day there's really nothing to

321
00:12:03,300 --> 00:12:07,680
do about the manufactured info leaks

322
00:12:05,459 --> 00:12:09,360
other than fix the underlying

323
00:12:07,680 --> 00:12:11,459
vulnerability that caused them so if

324
00:12:09,360 --> 00:12:13,860
they're using a heap overflow to create

325
00:12:11,459 --> 00:12:16,380
a ACID pointer you need to fix the heap

326
00:12:13,860 --> 00:12:18,240
overflow there's nothing specifically to

327
00:12:16,380 --> 00:12:19,860
fix about the info leak other than the

328
00:12:18,240 --> 00:12:22,140
underlying root cause and with that

329
00:12:19,860 --> 00:12:23,459
let's go look at how some of the exact

330
00:12:22,140 --> 00:12:24,959
same problems that you've seen

331
00:12:23,459 --> 00:12:27,600
throughout this class in the previous

332
00:12:24,959 --> 00:12:30,480
class can ultimately lead to information

333
00:12:27,600 --> 00:12:34,940
disclosure instead of just typical or

334
00:12:30,480 --> 00:12:34,940
memory writes or code execution

