1
00:00:00,060 --> 00:00:05,759
and now type confusions what are they

2
00:00:02,820 --> 00:00:08,760
they're the use of ACID to alter the

3
00:00:05,759 --> 00:00:11,880
interpretation of a data's type

4
00:00:08,760 --> 00:00:14,820
now me US guy I no like lot word chunk

5
00:00:11,880 --> 00:00:17,820
so instead of type confusion we're going

6
00:00:14,820 --> 00:00:20,220
to call it TyCo TyCo vulnerabilities

7
00:00:17,820 --> 00:00:21,960
and TyCo might make you think of these

8
00:00:20,220 --> 00:00:24,000
remote controlled cars from the 90s

9
00:00:21,960 --> 00:00:26,580
where nothing was cooler than looking

10
00:00:24,000 --> 00:00:29,160
like a rat with sunglasses and a

11
00:00:26,580 --> 00:00:32,340
backwards cap wearing sneakers and a

12
00:00:29,160 --> 00:00:34,620
t-shirt over a t-shirt or Tyco may make

13
00:00:32,340 --> 00:00:36,899
you think of the Tycho crater named

14
00:00:34,620 --> 00:00:38,700
after Tycho Brahe of course us English

15
00:00:36,899 --> 00:00:40,800
speakers like to mispronounce this as

16
00:00:38,700 --> 00:00:42,960
Tycho which I recently learned when I

17
00:00:40,800 --> 00:00:44,879
had a student named Tycho in one of my

18
00:00:42,960 --> 00:00:47,219
classes but that's neither here nor

19
00:00:44,879 --> 00:00:50,520
there what is here is the fact that

20
00:00:47,219 --> 00:00:55,739
apparently his name wasn't even Tycho it

21
00:00:50,520 --> 00:00:57,000
was Tyge Ottesen Brahe or Brahe I don't

22
00:00:55,739 --> 00:00:57,899
know whether it's "bra-he" or "bra" because

23
00:00:57,000 --> 00:01:00,420
there's all sorts of different

24
00:00:57,899 --> 00:01:02,820
pronunciations on the internet anyways

25
00:01:00,420 --> 00:01:05,519
so what are the common causes of TyCo

26
00:01:02,820 --> 00:01:07,140
vulnerabilities well one of them is if

27
00:01:05,519 --> 00:01:08,640
you're designing in such a way that

28
00:01:07,140 --> 00:01:11,159
there's some particular field that

29
00:01:08,640 --> 00:01:13,260
indicates the data's type then if an

30
00:01:11,159 --> 00:01:15,240
attacker can manipulate that field that

31
00:01:13,260 --> 00:01:17,040
would allow for type confusion and

32
00:01:15,240 --> 00:01:19,799
object-oriented programming is really

33
00:01:17,040 --> 00:01:22,979
just a subtype of this particular type

34
00:01:19,799 --> 00:01:25,920
of problem because in object-oriented C++

35
00:01:22,979 --> 00:01:28,380
you have the interpretation of

36
00:01:25,920 --> 00:01:30,840
data according to types having to do

37
00:01:28,380 --> 00:01:32,880
with complicated class hierarchies of

38
00:01:30,840 --> 00:01:35,280
parents and children and multiple

39
00:01:32,880 --> 00:01:37,680
inheritance and things like that so

40
00:01:35,280 --> 00:01:39,780
C++ code very often has type

41
00:01:37,680 --> 00:01:41,100
confusion vulnerabilities if the

42
00:01:39,780 --> 00:01:42,240
attacker can manipulate the

43
00:01:41,100 --> 00:01:44,880
interpretation

44
00:01:42,240 --> 00:01:47,280
now C++ has a variety of forms

45
00:01:44,880 --> 00:01:49,680
of type casting and of course you can

46
00:01:47,280 --> 00:01:52,259
still use the old C-style casting where

47
00:01:49,680 --> 00:01:55,619
you just cast a pointer to some new type

48
00:01:52,259 --> 00:01:58,020
and that is dangerous because if you do

49
00:01:55,619 --> 00:02:00,600
it incorrectly then the code will just

50
00:01:58,020 --> 00:02:03,240
be accessing data in the new type

51
00:02:00,600 --> 00:02:05,640
definition when it's not correct for the

52
00:02:03,240 --> 00:02:09,420
old type data but let's focus on the

53
00:02:05,640 --> 00:02:12,420
C++-isms here there is the static_cast

54
00:02:09,420 --> 00:02:15,060
which takes a target class that it's type

55
00:02:12,420 --> 00:02:17,520
casting it to and some particular object

56
00:02:15,060 --> 00:02:19,980
of some other class now this is

57
00:02:17,520 --> 00:02:23,280
theoretically checked at compile time

58
00:02:19,980 --> 00:02:25,379
but not at runtime and I have a quotes

59
00:02:23,280 --> 00:02:27,540
around checked because as you'll see in

60
00:02:25,379 --> 00:02:29,819
some examples here in a second the

61
00:02:27,540 --> 00:02:32,280
checks may be insufficient for avoiding

62
00:02:29,819 --> 00:02:34,140
actual type confusion then there's

63
00:02:32,280 --> 00:02:36,239
dynamic_cast and that's the thing that

64
00:02:34,140 --> 00:02:38,160
you should actually be using it is

65
00:02:36,239 --> 00:02:39,840
checked at run time rather than compile

66
00:02:38,160 --> 00:02:41,940
time although there are a few checks at

67
00:02:39,840 --> 00:02:43,920
compile time as well but unfortunately a

68
00:02:41,940 --> 00:02:46,860
lot of people don't use Dynamic cast

69
00:02:43,920 --> 00:02:49,980
because it has performance overhead so

70
00:02:46,860 --> 00:02:52,319
this is safe but not everybody is using

71
00:02:49,980 --> 00:02:54,239
it so Dynamic cast you know if you take

72
00:02:52,319 --> 00:02:56,940
anything away from the C plus casting

73
00:02:54,239 --> 00:02:58,739
the only thing is use dynamicast and

74
00:02:56,940 --> 00:03:00,120
nothing else of course if you do that

75
00:02:58,739 --> 00:03:02,160
then you have to do performance

76
00:03:00,120 --> 00:03:04,080
evaluations of what the implications are

77
00:03:02,160 --> 00:03:06,900
and then finally there's reinterpret_cast

78
00:03:04,080 --> 00:03:10,379
which is similarly dangerous to

79
00:03:06,900 --> 00:03:12,420
static_cast so in general up-casting is

80
00:03:10,379 --> 00:03:13,980
safe because you're narrowing the

81
00:03:12,420 --> 00:03:15,720
definition of something so if you have

82
00:03:13,980 --> 00:03:18,060
an animal class and there's dogs and

83
00:03:15,720 --> 00:03:20,159
there's cats well then treating a dog as

84
00:03:18,060 --> 00:03:23,099
an animal is always going to be safe

85
00:03:20,159 --> 00:03:25,080
because it's basically using a subset of

86
00:03:23,099 --> 00:03:26,819
the data that is available for dog only

87
00:03:25,080 --> 00:03:29,280
that subset that's available as an

88
00:03:26,819 --> 00:03:31,319
animal but if you up-cast and then you

89
00:03:29,280 --> 00:03:34,080
down-cast that is where you get into

90
00:03:31,319 --> 00:03:36,840
trouble because technically it is legal

91
00:03:34,080 --> 00:03:39,720
to go from dog to animal and technically

92
00:03:36,840 --> 00:03:43,680
it is legal to go from animal to cat but

93
00:03:39,720 --> 00:03:46,200
if the source object was a cat and you

94
00:03:43,680 --> 00:03:48,840
static_cast it to an animal and then you

95
00:03:46,200 --> 00:03:50,519
static_cast it down to a dog while

96
00:03:48,840 --> 00:03:52,920
you've just forced a cat to be

97
00:03:50,519 --> 00:03:54,599
interpreted as a dog and although the

98
00:03:52,920 --> 00:03:58,080
compiler will say yeah going from an

99
00:03:54,599 --> 00:03:59,940
animal to a dog is totally fine the

100
00:03:58,080 --> 00:04:02,340
reality is this is going to cause

101
00:03:59,940 --> 00:04:06,659
problems because now you will have

102
00:04:02,340 --> 00:04:08,700
access to members of the dog class which

103
00:04:06,659 --> 00:04:11,580
may or may not exist or may not be the

104
00:04:08,700 --> 00:04:13,680
correct and same type from the cat class

105
00:04:11,580 --> 00:04:15,780
so let's see a quick real example of

106
00:04:13,680 --> 00:04:17,940
that let's say we have a base class with

107
00:04:15,780 --> 00:04:20,639
whole bunch of nothing going on then we

108
00:04:17,940 --> 00:04:22,860
have the Execute class as a subclass of

109
00:04:20,639 --> 00:04:26,340
base and all it does is take in a string

110
00:04:22,860 --> 00:04:29,520
and it calls system to execute that path

111
00:04:26,340 --> 00:04:31,919
as a executable then there's a Greeter

112
00:04:29,520 --> 00:04:34,139
class which is a subclass of base and

113
00:04:31,919 --> 00:04:36,479
all that does is taking a string and it

114
00:04:34,139 --> 00:04:38,880
prints out the string so main is going

115
00:04:36,479 --> 00:04:42,180
to instantiate a new Greeter object and

116
00:04:38,880 --> 00:04:43,860
store it in the base pointer b1 it's

117
00:04:42,180 --> 00:04:46,560
going to instantiate a new Execute

118
00:04:43,860 --> 00:04:49,560
object and it's going to store it in b2

119
00:04:46,560 --> 00:04:52,620
then it's got a Greeter pointer so it

120
00:04:49,560 --> 00:04:54,240
can take b1 which is of type Greeter and

121
00:04:52,620 --> 00:04:57,300
which was stored in the base class

122
00:04:54,240 --> 00:04:59,820
pointer and it can static_cast it to a

123
00:04:57,300 --> 00:05:01,620
Greeter and that is going to be safe and

124
00:04:59,820 --> 00:05:04,380
legal because it's just casting a

125
00:05:01,620 --> 00:05:08,100
Greeter to a Greeter but if it does a

126
00:05:04,380 --> 00:05:10,680
static_cast of b2 which was an Execute

127
00:05:08,100 --> 00:05:13,740
to a Greeter and then it calls the

128
00:05:10,680 --> 00:05:15,720
function sayHi from the greater well

129
00:05:13,740 --> 00:05:17,880
because now it has been type confused

130
00:05:15,720 --> 00:05:20,280
because it is turned in Execute class

131
00:05:17,880 --> 00:05:22,020
into a Greeter class there's only a

132
00:05:20,280 --> 00:05:23,940
single function in each of these and so

133
00:05:22,020 --> 00:05:25,680
instead of executing the function sayHi

134
00:05:23,940 --> 00:05:28,380
it's going to be executing the function

135
00:05:25,680 --> 00:05:30,840
exec behind the scenes so even though

136
00:05:28,380 --> 00:05:34,680
the code looks like it's saying hi it's

137
00:05:30,840 --> 00:05:37,080
actually executing this as a path to an

138
00:05:34,680 --> 00:05:40,080
executable and so this will pop open the

139
00:05:37,080 --> 00:05:42,360
calc if you run it on a Unix system

140
00:05:40,080 --> 00:05:44,460
and the perhaps surprising thing here is

141
00:05:42,360 --> 00:05:46,320
the fact that it allows this right this

142
00:05:44,460 --> 00:05:48,539
static_cast is supposed to have compile

143
00:05:46,320 --> 00:05:51,840
time checks right so why does it allow

144
00:05:48,539 --> 00:05:54,120
that well it allows that because b2 is a

145
00:05:51,840 --> 00:05:56,820
base it's a base class and so it's okay

146
00:05:54,120 --> 00:06:00,120
to cast from a base class to a subclass

147
00:05:56,820 --> 00:06:01,979
such as g the Greeter it's okay

148
00:06:00,120 --> 00:06:03,600
according to compilation rules but we

149
00:06:01,979 --> 00:06:05,699
know this is actually going to cause an

150
00:06:03,600 --> 00:06:07,440
issue so the problem here is that we

151
00:06:05,699 --> 00:06:09,240
have a Greeter object which has a

152
00:06:07,440 --> 00:06:11,400
virtual function and so the object

153
00:06:09,240 --> 00:06:13,380
itself there's no member so it's just a

154
00:06:11,400 --> 00:06:15,660
pointer to a table that says you know

155
00:06:13,380 --> 00:06:18,360
where you can find the function to the

156
00:06:15,660 --> 00:06:20,400
call so Greeter has a sayHi and Execute

157
00:06:18,360 --> 00:06:22,560
has an exec the problem is when they

158
00:06:20,400 --> 00:06:24,660
become type confused you think you're

159
00:06:22,560 --> 00:06:26,940
operating on a Greeter object but

160
00:06:24,660 --> 00:06:28,860
actually you're operating on an Execute

161
00:06:26,940 --> 00:06:31,800
object and therefore calling sayHi

162
00:06:28,860 --> 00:06:33,539
calls through to exec and so more

163
00:06:31,800 --> 00:06:35,460
generically you can have any sort of

164
00:06:33,539 --> 00:06:37,440
type confusion and this can be a problem

165
00:06:35,460 --> 00:06:39,539
either because of the overlapping of

166
00:06:37,440 --> 00:06:42,419
virtual functions or because of the

167
00:06:39,539 --> 00:06:44,819
overlapping of members so if this object

168
00:06:42,419 --> 00:06:47,280
type 1 is interpreted as object type 2

169
00:06:44,819 --> 00:06:49,500
then when you think you're accessing you

170
00:06:47,280 --> 00:06:51,600
know member4 of type 4 you're

171
00:06:49,500 --> 00:06:53,400
actually getting something of type 1 and

172
00:06:51,600 --> 00:06:55,380
that can cause a problem similarly

173
00:06:53,400 --> 00:06:57,300
you're calling the wrong functions and

174
00:06:55,380 --> 00:06:58,740
therefore the parameters to those

175
00:06:57,300 --> 00:07:01,199
functions may be interpreted as

176
00:06:58,740 --> 00:07:03,900
different types than what the actual

177
00:07:01,199 --> 00:07:06,180
data is so if we return to this example

178
00:07:03,900 --> 00:07:08,759
and we change it to be a dynamic_cast

179
00:07:06,180 --> 00:07:10,620
instead well then we'll actually get a

180
00:07:08,759 --> 00:07:12,240
compile time error so we said dynamic_casts

181
00:07:10,620 --> 00:07:14,220
are supposed to give you runtime

182
00:07:12,240 --> 00:07:16,319
errors and static_casts compile time

183
00:07:14,220 --> 00:07:18,300
errors so what's going on well it says

184
00:07:16,319 --> 00:07:20,520
that you know the source type is not

185
00:07:18,300 --> 00:07:23,280
polymorphic so if we had more

186
00:07:20,520 --> 00:07:25,259
complicated polymorphic classes then

187
00:07:23,280 --> 00:07:27,360
dynamic_cast wouldn't have a problem

188
00:07:25,259 --> 00:07:29,759
here at compile time it would just do

189
00:07:27,360 --> 00:07:31,740
the actual checks at runtime but this is

190
00:07:29,759 --> 00:07:33,840
good for us right it's always better if

191
00:07:31,740 --> 00:07:36,120
there's some sort of sanity check at

192
00:07:33,840 --> 00:07:38,340
compile time instead of runtime so again

193
00:07:36,120 --> 00:07:40,680
this is why dynamic_cast is the C++

194
00:07:38,340 --> 00:07:43,319
cast you want to use if if you can

195
00:07:40,680 --> 00:07:45,120
absorb the performance penalty and

196
00:07:43,319 --> 00:07:47,280
finally if we just want to see static_cast

197
00:07:45,120 --> 00:07:49,259
actually giving us a compile time check

198
00:07:47,280 --> 00:07:51,539
then we have to go a little bit out of

199
00:07:49,259 --> 00:07:55,380
our way for this trivial example create

200
00:07:51,539 --> 00:07:57,660
a new base class to Base2 and then we

201
00:07:55,380 --> 00:08:01,259
have Greeter2 which is a subtype

202
00:07:57,660 --> 00:08:03,479
subclass of Base2 and then we create a

203
00:08:01,259 --> 00:08:06,660
Greeter2 and we explicitly cast

204
00:08:03,479 --> 00:08:09,360
Greeter2 into a Greeter and so that

205
00:08:06,660 --> 00:08:11,759
will at least give us an actual compile

206
00:08:09,360 --> 00:08:14,520
time check so it's going to say invalid

207
00:08:11,759 --> 00:08:16,020
static_cast from type Base2 to type

208
00:08:14,520 --> 00:08:18,060
Greeter

209
00:08:16,020 --> 00:08:20,340
all right so I want you to bear with us

210
00:08:18,060 --> 00:08:21,960
in this section I thought that it was

211
00:08:20,340 --> 00:08:24,120
more important for this particular

212
00:08:21,960 --> 00:08:26,639
section for you to be able to see the

213
00:08:24,120 --> 00:08:28,440
recurring patterns rather than spreading

214
00:08:26,639 --> 00:08:29,759
these examples all over the place in

215
00:08:28,440 --> 00:08:32,700
terms of userspace kernelspace

216
00:08:29,759 --> 00:08:34,380
firmware and virtualization so in this

217
00:08:32,700 --> 00:08:36,539
particular thing the examples that were

218
00:08:34,380 --> 00:08:38,520
easiest to find and that you would have

219
00:08:36,539 --> 00:08:40,800
the appropriate background for are

220
00:08:38,520 --> 00:08:42,719
mostly kernel examples so it's going to

221
00:08:40,800 --> 00:08:44,940
be kernel example heavy here and we'll

222
00:08:42,719 --> 00:08:47,100
try to increase the number of examples

223
00:08:44,940 --> 00:08:49,200
in the future to have other places

224
00:08:47,100 --> 00:08:50,880
and you know we have very much a

225
00:08:49,200 --> 00:08:53,160
selection bias it's not just the

226
00:08:50,880 --> 00:08:55,140
choosing of kernel examples one of the

227
00:08:53,160 --> 00:08:57,660
other major sources of type confusion

228
00:08:55,140 --> 00:08:59,580
vulnerabilities are browsers and so

229
00:08:57,660 --> 00:09:01,680
browsers typically have some sort of

230
00:08:59,580 --> 00:09:04,260
problem in their JavaScript interpreter

231
00:09:01,680 --> 00:09:07,200
dealing with these objects that

232
00:09:04,260 --> 00:09:09,120
JavaScript manages but because we can't

233
00:09:07,200 --> 00:09:11,580
assume you know assembly we can't assume

234
00:09:09,120 --> 00:09:14,279
you know JavaScript it would just take

235
00:09:11,580 --> 00:09:17,220
way too much time to explain all of the

236
00:09:14,279 --> 00:09:19,320
background about the the necessary

237
00:09:17,220 --> 00:09:21,060
background about JavaScript in order for

238
00:09:19,320 --> 00:09:22,680
you to understand it and even me

239
00:09:21,060 --> 00:09:24,300
personally I don't know JavaScript that

240
00:09:22,680 --> 00:09:26,279
well so it would take me a ton of time

241
00:09:24,300 --> 00:09:27,480
in order to understand it well enough to

242
00:09:26,279 --> 00:09:29,640
explain it well enough for you to

243
00:09:27,480 --> 00:09:31,080
understand so again we're going to be

244
00:09:29,640 --> 00:09:33,000
leaving those types of vulnerabilities

245
00:09:31,080 --> 00:09:35,100
out of the class for now

246
00:09:33,000 --> 00:09:38,600
but enough with the caveats let's go

247
00:09:35,100 --> 00:09:38,600
look at some real vulnerabilities

