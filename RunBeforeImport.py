# I didn't write this code, ChatGPT did :)

import os
import re

def update_filenames(root_path):
    # Define the pattern to match filenames
    pattern = r"__v(\d+)-en\.srt$"
    regex = re.compile(pattern)

    # Walk through the directory and its subdirectories
    for dirpath, dirnames, filenames in os.walk(root_path):
        for filename in filenames:
            # Check if the filename matches the pattern
            match = regex.search(filename)
            if match is not None:
                # Extract the number from the filename
                num = int(match.group(1))

                # Increment the number and update the filename
                new_num = num + 1
                new_filename = regex.sub("__v{}-en.srt".format(new_num), filename)

                # Rename the file
                old_path = os.path.join(dirpath, filename)
                new_path = os.path.join(dirpath, new_filename)
                os.rename(old_path, new_path)

# Call the function to update the filenames in a directory
update_filenames("course/static/")

###

def increment_strings_within_xml(root_path):
    # Define the pattern to match strings like "__v1"
    pattern = r"(__v\d+)\b"
    regex = re.compile(pattern)

    # Walk through the directory and its subdirectories
    for dirpath, dirnames, filenames in os.walk(root_path):
        for filename in filenames:
            # Check if the file is an XML file
            if filename.endswith(".xml"):
                file_path = os.path.join(dirpath, filename)

                # Open the file for reading and writing
                with open(file_path, "r+") as f:
                    # Read the file content into memory
                    lines = f.readlines()

                    # Iterate over each line in the file
                    for i, line in enumerate(lines):
                        # Use regex to find matches in the line
                        matches = regex.findall(line)

                        # Iterate over each match and increment the number
                        for match in matches:
                            num = int(match[3:])  # Extract the number from the match
                            new_num = num + 1
                            new_match = "__v" + str(new_num)
                            line = line.replace(match, new_match)

                        # Update the line in the list of lines
                        lines[i] = line

                    # Write the updated lines back to the file
                    f.seek(0)
                    f.writelines(lines)
                    f.truncate()

# Call the function to increment the numbers in a directory
increment_strings_within_xml("course/video/")

