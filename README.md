**Subtitle fix contributors can find the subtitles in *course/static/***

---
Notes to self:  

*Usage for InitialSubtitleRename.py*:  
When a course is first exported from Open edX and decompressed, one runs:  
```
python3 InitialSubtitleRename.py <path_to_subtitles_to_match>
```
This will replace the GUIDed-named files with human-readable file names from <path_to_subtitles_to_match>. It will also update files in course/video/*.xml that may contain references to the old GUID.

This script can be run multiple files on multiple paths, if the source subtitles are spread around. But ultimately it only needs to be run once before the first git checkin.

---

*Usage for RunBeforeImport.py*:  

* Case 1: There have been no changes to the contents of the web page between last check in, and new subtitle update:  

* * After a merge request occurs that changes the subtitles, the local git repository should be updated via a pull

* * python3 RunBeforeImport.py can be run, at which point it will update all the files named like __v1 to __v2. It will also update references within XML files.

* Case 2: There have been changes to the contents of the web page between last check in, and new subtitle update:  

* * First use the Open edX Export functionality from the instructor studio class page.

* * Move the old "course" folder out of the way, and rename all subtitles in course/static to remove the "__v1" (or whatever the number currently is)

* * Expand the newly exported course, and run the InitialSubtitleRename.py script on it as defined above, but using the old course/static path as the input to InitialSubtitleRename.py

* * Run "python3 RunBeforeImport.py" to update the version numbers (e.g. __v1 -> __v2, __v2 -> __v3, etc)

* * Add all files in course (which will include new files if the course has had any content added)

* * Check the course in


Run ```tar czvf upload.tar.gz course``` and then use the Import functionality in Open edX to import upload.tar.gz and overwrite the subtitles for the class.
